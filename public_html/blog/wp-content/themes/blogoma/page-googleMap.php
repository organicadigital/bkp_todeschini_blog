
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Hagu
 * Template name: Contact - With Google Map
 */
	
	get_header(); 	

	$page_id     		= get_queried_object_id();
	
	/* GOOGLE MAP VALUE */
	$options 				= get_option('blogoma_admin'); 
	$google_map				= $options['use-google-map'];
	
	$google_map				= true;

	$lat					= $options['latitude'];
	$long					= $options['longitude'];
	$zoom_level				= $options['zoom-level'];
	$enable_zoom			= $options['enable-zoom'];
	$custom_marker			= $options['marker-img'];
	$enable_animation		= $options['enable-animation'];
	
	wp_register_script('google-map', 'http://maps.google.com/maps/api/js?sensor=false', NULL, NULL, TRUE);
	wp_register_script('map-js', get_template_directory_uri() . '/js/map.js', array('jquery', 'google-map'), '1.0', TRUE);
	
	wp_enqueue_script('google-map');
	wp_enqueue_script('map-js');
	
?>
	
<?php if($google_map) : ?>
	<div id="google-map" data-lat="<?php echo esc_attr($lat); ?>" data-long="<?php echo esc_attr($long); ?>" data-zoom-level="<?php echo esc_attr($zoom_level); ?>"  data-enable-zoom="<?php echo esc_attr($enable_zoom); ?>" data-custom-marker="<?php echo esc_attr($custom_marker); ?>" data-enable-animation="<?php echo esc_attr($enable_animation); ?>" >
		<!-- GOOGLE MAP -->
	</div>
<?php endif; ?>
<div class="row">
	<div class="container">
		<div class="col-md-12">		
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<div class="contact-holder">
					<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php endwhile; // end of the loop. ?>
					</div>
				</main><!-- #main -->
				<div class="post-paper-bg full"></div>
			</div><!-- #primary -->
		</div>
	</div>
</div>


<?php get_footer(); ?>

