<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package blogoxma
 */

get_header(); 

$options 			= get_option('blogoma_admin'); 
$blogoma_paging		= $options['blogoma_paging'];

?>
<div class="row">
	<div class="container">
		<div class="col-md-8">
			
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'blogoxma' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
					get_template_part( 'inc/post-formats/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php if($blogoma_paging) : ?>
				<?php blogoma_numeric_paging_nav(); ?>
			<?php else : ?>
				<?php blogoma_paging_nav(); ?>
			<?php endif; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->
		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
