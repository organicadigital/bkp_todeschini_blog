<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package blogoma
 */

get_header(); ?>
<div class="row">
	<div class="container">
		<div class="col-md-12">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<section class="error-404 not-found">
						<header class="page-header">
							<img class="title-404" src="<?php echo get_template_directory_uri()."/images/404-title.png" ?>" alt="404" />
							<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'blogoma' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content">
							<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'blogoma' ); ?></p>

							<div class="row">
									<div class="col-md-6 col-md-offset-3">
										<?php get_search_form(); ?>
								</div>
							</div>

						</div><!-- .page-content -->
					</section><!-- .error-404 -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
	</div>
</div>
<?php get_footer(); ?>