<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package blogoma
 */

/*
Template Name: Archives
*/

get_header(); 


$options 			= get_option('blogoma_admin'); 
$blogoma_paging		= $options['blogoma_paging'];

?>
<div class="row">
	<div class="container">
		<div class="col-md-8">
			
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) :
							_e( 'Categories: ', 'blogoma' ). single_cat_title();

						elseif ( is_tag() ) :
							_e( 'Tags: ', 'blogoma' ). single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'blogoma' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'blogoma' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'blogoma' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'blogoma' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'blogoma' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'blogoma' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'blogoma' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'blogoma');

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'blogoma');

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'blogoma' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'blogoma' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'blogoma' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'blogoma' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'blogoma' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'blogoma' );

						else :
							_e( 'Archives', 'blogoma' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'inc/post-formats/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php if($blogoma_paging) : ?>
							<?php blogoma_numeric_paging_nav(); ?>
						<?php else : ?>
							<?php blogoma_paging_nav(); ?>
						<?php endif; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->
		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
