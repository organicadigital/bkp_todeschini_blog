<?php 
	$options = get_option('blogoma_admin'); 

	if(!empty($options['custom_fonts']) && $options['custom_fonts'] == 1 ) {
		$body = $options['main_font'];
		$search = $options['second_font'];
		$headings = $options['heading_font'];
		$quote = $options['quote_font'];
		$code = $options['code_font'];
	}
	else
	{
		$all_font = $options['all_font'];
	}
	
	if(!empty($options['custom_typo']) && $options['custom_typo'] == 1 ) {
		$body_font_size 			= $options['body_font_size'];
		$page_title_font_size		= $options['page_title_font_size'];
		$h1_font_size 				= $options['h1_font_size'];
		$h2_font_size 				= $options['h2_font_size'];
		$h3_font_size 				= $options['h3_font_size'];
		$h4_font_size 				= $options['h4_font_size'];
		$h5_font_size 				= $options['h5_font_size'];
		$quote_font_size 			= $options['quote_title_font_size'];
		$code_font_size 			= $options['code_font_size'];
	}
?>
<?php if(!empty($options['custom_fonts']) && $options['custom_fonts'] == 1 ) : ?>
/* 
DONT CHANGE ANYTHING! IT'S AUTO GENERATED CSS FILE
They gets value from themetica admin options panel. 
*/
	
	/* Google Fonts */
	
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, font, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	.quote-holder blockquote cite {
		font-family: <?php echo $body; ?>;
	}
	
	h1, h2, h3, h4, h5, h6,
	.entry-title a{
		font-family: <?php echo $headings; ?>;
	}

	.link-holder a,
	.link-holder span,
	.quote-holder blockquote{
		font-family: <?php echo $quote; ?>;
	}



	.search-holder .search-field,
	.widget_search .search-field{
		font-family: <?php echo $search; ?>;
	}

	.code-quote{
		font-family: <?php echo $code; ?>;
	}

<?php else : ?>
	/* Standart Fonts */
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, font, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	.quote-holder blockquote cite {
		font-family: 'Open Sans', sans-serif;
	}
<?php endif; ?>
	
<?php if(!empty($options['custom_typo']) && $options['custom_typo'] == 1 ) : ?>
	/* Typo */
	body{
		font-size: <?php echo $body_font_size+9 . "px" ?>;
	}
	.link-holder a, .quote-holder blockquote{
		font-size: <?php echo $quote_font_size+9 . "px" ?>;
	}
	h1,
	.entry-title a{
		font-size: <?php echo $h1_font_size+9 . "px" ?>;
		line-height: <?php echo $h1_font_size+9 . "px" ?>;
	}
	h2{
		font-size: <?php echo $h2_font_size+9 . "px" ?>;
	}
	h3{
		font-size: <?php echo $h3_font_size+9 . "px" ?>;
	}
	h4{
		font-size: <?php echo $h4_font_size+9 . "px" ?>;
	}
	h5{
		font-size: <?php echo $h5_font_size+9 . "px" ?>;
	}

<?php else : ?>

		/* Standard Typo */
	body{
		font-size: <?php echo $body_font_size+9 . "px" ?>;
	}
	.link-holder a, .quote-holder blockquote{
		font-size: <?php echo $quote_font_size+9 . "px" ?>;
	}
	h1,
	.entry-title a{
		font-size: 38px ?>;
		line-height: 1 ?>;
	}
	h2{
		font-size: <?php echo $h2_font_size+9 . "px" ?>;
	}
	h3{
		font-size: <?php echo $h3_font_size+9 . "px" ?>;
	}
	h4{
		font-size: <?php echo $h4_font_size+9 . "px" ?>;
	}
	h5{
		font-size: <?php echo $h5_font_size+9 . "px" ?>;
	}
	p{
	    margin-bottom: 1.7em;
		line-height: 1.5em;
	}
	.search-form .search-field{
		font-size: 16px;
	}
	.awesome-social {
		font-size: 1.5em !important;
	}
	
<?php endif; ?>