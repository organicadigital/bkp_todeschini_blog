<?php
$options 				= get_option('blogoma_admin'); 

$custom_color 			= $options['theme_color'];

$custom_header_color 	= $options['site_header_color'];

$custom_site_color 		= $options['site_bg_color'];

$custom_footer_color 	= $options['footer_bg_color'];

$logo_padding		= $options['logo-padding'];
$menu_padding		= $options['menu-item-padding'];
$search_padding		= $options['search-item-padding'];

/* 
DONT CHANGE ANYTHING! IT'S AUTO GENERATED CSS FILE
They gets value from themetica admin options panel. 
*/

?>
.site-branding{
	padding: <?php echo $logo_padding; ?>;
}

.main-navigation ul li{
	padding: <?php echo $menu_padding; ?>;
}

.site-search{
	padding: <?php echo $search_padding; ?>;
}

.featured-entry-title a:hover,
.widget a:hover,
.widget h2,
.entry-content a,
a:hover,
a:focus,
a:active {
	color: <?php echo $custom_color; ?>;
}
.main-navigation ul li:hover > a, .main-navigation .current_page_item a, .main-navigation .current-menu-item a{
	border-color: #FFF;
	color: #F3F3F3;
}
.main-navigation ul li a{
	color: #FFF;
	border: 1px solid transparent;
	padding: 10px 15px;
}

.main-navigation ul ul {
	background: <?php echo $custom_color; ?>;
}

.widget_categories ul li:hover span{
	<?php 
		$color = $custom_color;
		$rbga = hex2rgba($color, 1);
	?>
	background: <?php echo $rbga; ?>;;
	border: <?php echo $custom_color; ?>;
	opacity:1;
}

.widget_categories ul li:hover a{
	color: #FFF;
	border: 2px solid <?php echo $custom_color; ?>;
}
.widget_categories ul li a{
	padding: 15px 10px;
}

ul.tabs-nav li a{
	color: <?php echo $custom_color; ?>;
}

.recent_tabs a:hover{
	color: <?php echo $custom_color; ?>;
}

.recent_tabs .tags-tab a:hover,
.tagcloud a:hover{
	border: 2px solid <?php echo $custom_color; ?>;
	background: <?php echo $custom_color; ?>;
	color:#fff;
}

.entry-title a{
	color: <?php echo $custom_color; ?>;
}

.link-holder .bg,
.quote-holder .bg{
	background: <?php echo $custom_color; ?>;
}

.link-holder a,
.link-holder span{
	color: #fff;
}

.numeric-nav ul li.active{
	
}

.numeric-nav ul li a:hover{
	color: <?php echo $custom_color; ?>;
}

.numeric-nav ul li.prev-btn:hover,
.numeric-nav ul li.next-btn:hover{
	background: transparent !important;
	border: 1px solid transparent !important;
	color: <?php echo $custom_color; ?>;
}

.numeric-nav ul li.prev-btn:hover a,
.numeric-nav ul li.next-btn:hover a{
	color: <?php echo $custom_color; ?>;
}

.numeric-nav ul li:hover a, 
.numeric-nav ul li.active a {
	color: <?php echo $custom_color; ?>;
}

.nav-next a,
.nav-previous a{
	color: <?php echo $custom_color; ?>;
}

.nav-next a:hover,
.nav-previous a:hover{
	background: <?php echo $custom_color; ?>;
	color: #fff;
}

.search-holder .search-field{
	color: <?php echo $custom_color; ?>;
}


.search-holder .search-field{
	color: <?php echo $custom_color; ?>;
}

.copy-info a:hover{
	color: <?php echo $custom_color; ?>;
}

.mejs-controls .mejs-volume-button .mejs-volume-slider .mejs-volume-current,
.mejs-controls .mejs-time-rail .mejs-time-current,
.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current{
	background:<?php echo $custom_color; ?>;
}

.mobile-navigation a{
	color: #444;
}

.mobile-navigation a.active{
	color: <?php echo $custom_color; ?>;
}

span.circle,
span.square{
	background: <?php echo $custom_color; ?> !important;
}

.mobile-navigation ul li ul li a{
	background: <?php echo $custom_color; ?> !important;
}


.highlight{
	background: <?php echo $custom_color; ?> !important;
}

.blockquote-holder{
	color:<?php echo $custom_color; ?> !important;
}


blockquote{
	border-left: 2px solid <?php echo $custom_color; ?>;
}

.entry-footer .tags-links a{
	color: <?php echo $custom_color; ?>;
   	border-radius: 0;
}
.entry-footer .tags-links a:hover{
	border: 2px solid <?php echo $custom_color; ?>;
	background: <?php echo $custom_color; ?>;
}

.author-holder .info h3{
	color: <?php echo $custom_color; ?>;
}

.related-post-holder h3{
	color: <?php echo $custom_color; ?>;
}

.related-post-holder ul li .related-content{
	<?php 
		$color = $custom_color;
		$rbga = hex2rgba($color, 0.75);
	?>
	background: <?php echo $rbga; ?>;
}

.comment-author-info cite,
.comment-author-info cite a,
.comment-notes,
.comment-reply-title,
.comments-area h2{
	color: <?php echo $custom_color; ?>;
}

.reply a{
	color: <?php echo $custom_color; ?>;
}

.reply a:hover{
	border: 2px solid  <?php echo $custom_color; ?>;
	background:  <?php echo $custom_color; ?>;
}

.page-header{
	color: <?php echo $custom_color; ?>;
}

.social-widget ul li a:hover{
	color: <?php echo $custom_color; ?>;
}

.social-media-icons ul li a:hover{
	color: <?php echo $custom_color; ?>;
}

input[type="text"]:focus,
input[type="email"]:focus,
input[type="url"]:focus,
input[type="password"]:focus,
input[type="search"]:focus,
textarea:focus {
	color: <?php echo $custom_color; ?>;
}

input[type="submit"]{
	background: <?php echo $custom_color; ?>;
	color: #fff;
}

input[type="submit"]:hover{
	color: #fff;
	background: #444;
	border-color: #444;
}

.read-more a:hover, .social-widgets-holder a:hover{
       color: <?php echo $custom_color; ?>!important;
}

input[type="submit"]:hover{
   border: <?php echo $custom_color; ?>!important;
}

ul.tabs-nav li.active a,
ul.tabs-nav li.active a:hover{
	color: <?php echo $custom_color; ?>;
}

.recent_tabs .tags-tab a, .tagcloud a{
	color: #444444;
}



.recent_tabs .tabs-nav a:hover{
	color: <?php echo $custom_color; ?>;
}

.widget_categories a:hover{
	color: <?php echo $custom_color; ?>;
}


.social-post-embed-holder{
	background-color:<?php echo $custom_color; ?>;
}

.recent_tabs a,
.widget_categories a,
ul.tabs-nav li a{
	color : #444;
}

.main-navigation ul ul li:hover a,
.main-navigation ul ul a:hover{
	color: rgba(255, 255, 255, 0.70);
}

.entry-meta a {
	color: rgba(152, 152, 152, 0.75);
}

.site-header{
	background: <?php echo $custom_header_color; ?>;
}

.site{
	background: <?php echo $custom_site_color; ?>;
}

.site-footer{
	background: <?php echo $custom_footer_color; ?>;
}

.flex-direction-nav .flex-prev,
.flex-direction-nav .flex-next{
	background-color: <?php echo $custom_color; ?>;
}

#calendar_wrap caption{
	color: <?php echo $custom_color; ?>;
}

input[placeholder], [placeholder], *[placeholder]{
	color: <?php echo $custom_color; ?> !important;
}

.awesome-social:hover {
	color: <?php echo $custom_color; ?>!important;
}


<?php 
	function hex2rgba($color, $opacity = false) {
		$default = 'rgb(0,0,0)';

		//Return default if no color provided
		if(empty($color))
	          return $default; 

		//Sanitize $color if "#" is provided 
	        if ($color[0] == '#' ) {
	        	$color = substr( $color, 1 );
	        }

	        //Check if color has 6 or 3 characters and get values
	        if (strlen($color) == 6) {
	                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	        } elseif ( strlen( $color ) == 3 ) {
	                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	        } else {
	                return $default;
	        }

	        //Convert hexadec to rgb
	        $rgb =  array_map('hexdec', $hex);

	        //Check if opacity is set(rgba or rgb)
	        if($opacity){
	        	if(abs($opacity) > 1)
	        		$opacity = 1.0;
	        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
	        } else {
	        	$output = 'rgb('.implode(",",$rgb).')';
	        }

	        //Return rgb(a) color string
	        return $output;
	}
?>