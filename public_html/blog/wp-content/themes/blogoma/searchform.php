<form role="search" method="get" id="searchform" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label>
    	<span class="screen-reader-text" for="s">Search for:</span>
        <input class="search-field" type="text" value="" placeholder="Digite sua pesquisa" name="s" id="s" />
    </label>
    <input type="submit" class="search-submit" value="Search">
</form>