/* bootstrap-transition.js v2.1.1
 * http://twitter.github.com/bootstrap/javascript.html#transitions
 * ===================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */

document.createElement("header");  
document.createElement("footer");  
document.createElement("article");  

!function ($) {

  $(function () {

    "use strict"; // jshint ;_;


    /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
     * ======================================================= */

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }

    })()

  });

}(window.jQuery);


jQuery(document).ready(function($) {
  "use strict"; // jshint ;_;

  $(".featured-item" ).last().addClass("featured-last");
  $(".featured-posts").show();

  $('video,audio').mediaelementplayer(/* Options */);

  // Superfish
  $('.main-navigation ul').superfish({
      animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
      animationOut:  {opacity:'hide',height:'hide'},
      speed:       'fast',                          // faster animation speed
      autoArrows:  false   
  });


   // Hide empty entry-content div
   $('.entry-content').each(function(){
      if($.trim($(this).text()).length < 1){
        $(this).hide();
      }
   });

   /* THEMETICA RECENT TABS */
   $(".recent_tabs").find(".tabs-nav a").click(function(){
       var _href= $(this).attr("data-tab-href");
       _href = "."+_href + "-tab";
      
       // Change Active Tab
       $(".tabs-nav").find(".active").removeClass("active");
       $(this).parent().addClass("active");

       // Display Active List
       $(".recent_tabs .tab").removeClass("show_tab");
       $(".recent_tabs").find(_href).addClass("show_tab");
   });

   //init tab
   $(".tabs-nav a").eq(0).click();


   $(".site-search .search-btn").click(function(){
      $(".search-holder").fadeIn(400);
   });

   $(".search-holder .close-btn").click(function(){
      $(".search-holder").hide()
   });

   $(window).load(function(){
      $(".related-content").each(function(){
          $(this).css('line-height', $(this).height() + "px");
      });
   });

   $(window).resize(function(){
      $(".related-content").each(function(){
          $(this).css('line-height', $(this).height() + "px");
      });
   });


   //footer social icon center
   var social_ul_width = 0;
  
   $("footer .social-media-icons li").each(function(){
       social_ul_width += $(this).outerWidth();
   })
  
   if($(window).width() <= 768 && $(window).width() >= 320 ){
         social_ul_width = 250;
    }
  
  $("footer .social-media-icons ul").width(social_ul_width);

  //$(".mobile-menu ").height($(".site-branding").height());
  
    $(".mobile-menu a").click(function(){
      $(".mobile-navigation").slideToggle("fast", function(){}); 
    });

   $(document).keyup(function(e) {
      if (e.keyCode == 27) { 
        $(".search-holder .close-btn").click(); 
      }   // esc
    });

   $('.flexslider').each(function(){
      var $this = $(this);
    
      $this.flexslider({
        animation: 'slide',
        prevText: "Prev",
        nextText: "Next",
        smoothHeight: true
        });
    });

   var slider_nav_margin_value =  -1 * ($(".featured-slider-holder").find(".flex-control-nav").width() / 2);
   $(".featured-slider-holder").find(".flex-control-nav").css("margin-left", slider_nav_margin_value+"px");

   var blog_type = $(".content-area").attr("data-blog-type");

    $(".mobile-navigation a").on('click touchstart', function () {
      $(this).next("ul").slideToggle();
      $(this).toggleClass("active");
      if($(this).next("ul").length > 0){
        return false; 
      }
    });

    var $headerSticky       =  parseInt($(".site-header").attr("data-header-sticky"));

    if($headerSticky){
      if($(window).width() > 768 ){
        $(".site-header").addClass("is-sticky");
        $(".site-content").css("padding-top", $(".site-header").height() - 1);
      }
    }
    

});