jQuery(document).ready(function($) {
 
    "use strict"; // jshint ;_;

    $(".post-share a.share-btn").click(function(){
        //console.log("post share click");
        $(this).parent().toggleClass("active");
        $(this).parent().find(".share-list").fadeToggle();
        return false;
    })

    $(document).click(function() {
        $(".post-share .share-list").hide();
        $(".post-share").removeClass("active");
    });
})