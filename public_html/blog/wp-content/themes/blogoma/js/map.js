jQuery(document).ready(function($){

	
    var lat = parseFloat($('#google-map').attr('data-lat'));
	var lng = parseFloat($('#google-map').attr('data-long'));
    var zoom_level = parseFloat($('#google-map').attr('data-zoom-level'));
	var enable_zoom = $('#google-map').attr('data-enable-zoom');
	var marker_img = $('#google-map').attr('data-custom-marker');
	var enable_animation = $('#google-map').attr('data-enable-animation');
	var animationDelay = 0; 
	
	if(marker_img == ""){
		marker_img = "//chart.googleapis.com/chart?chst=d_map_pin_letter_withshadow&chld=A|FE7569|000000";
	}
	
	if( isNaN(zoom_level) || zoom_level == 1 ) { zoom_level = 11;}
	if( isNaN(lat) ) { lat = 40.714623;}
	if( isNaN(lng) ) { lng = -74.006605;}
	
	if( typeof enable_animation != 'undefined' && enable_animation == 1 && $(window).width() > 690) { animationDelay = 120; enable_animation = google.maps.Animation.BOUNCE } else { enable_animation = null; }

    var lat_lng = new google.maps.LatLng(lat,lng);    
    
	var mapOptions = {
      center: lat_lng,
      zoom: zoom_level,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false,
      panControl: false,
	  zoomControl: enable_zoom,	  
	  zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.LEFT_CENTER
   	  },
	  mapTypeControl: false,
	  scaleControl: false,
	  streetViewControl: false
	  
    };
	
	var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);
	
	var infoWindows = [];
	
	google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
		
		//don't start the animation until the marker image is loaded if there is one
		if(marker_img != "") {
			var markerImgLoad = new Image();
			markerImgLoad.src = marker_img;
			
			$(markerImgLoad).load(function(){
				 setMarkers(map);
			});
		}
		else {
			setMarkers(map);
		}
    });
    
    
    function setMarkers(map) {
	
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: map,
			animation: enable_animation,
			icon: marker_img,
			optimized: false
		  });
				  
				  /*
		for (var i = 1; i <= Object.keys(map_data).length; i++) {  
			
			(function(i) {
				setTimeout(function() {
				
			      var marker = new google.maps.Marker({
			      	position: new google.maps.LatLng(map_data[i].lat, map_data[i].lng),
			        map: map,
					infoWindowIndex : i - 1,
					animation: enableAnimation,
					icon: markerImg,
					optimized: false
			      });
				  
				  setTimeout(function(){marker.setAnimation(null);},200);
				  
			      //infowindows 
			      var infowindow = new google.maps.InfoWindow({
			   	    content: map_data[i].mapinfo,
			    	maxWidth: 300
				  });
				  
				  infoWindows.push(infowindow);
			      
			      google.maps.event.addListener(marker, 'click', (function(marker, i) {
			        return function() {
			        	infoWindows[this.infoWindowIndex].open(map, this);
			        }
			        
			      })(marker, i));
		     	
		         }, i * animationDelay);
		         
		         
		     }(i));
		     

		 }//end for loop
		 */
	}//setMarker
	
});
