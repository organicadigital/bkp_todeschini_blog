<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package blogoma
 */

get_header();

$options 			= get_option('blogoma_admin'); 
$layout				= $options['blog_layout'];

$layout				= $options['blog_layout'];
$blog_type			= "normal";
$blogoma_paging		= $options['blogoma_paging'];
$featured_posts		= $options['featured_post'];
?>
	<?php if($featured_posts) : ?>
			<div class="featured-posts">
			<?php
				get_template_part( '/inc/featured', 'posts' );
			?>
		</div>
	<?php endif; ?>
		<div class="row">
			<div class="container">
					
				<?php if($layout == "left" && $blog_type == "normal"): ?>
					<div class="col-md-4 col-sm-5 col-xs-12">
						<?php get_sidebar(); ?>
					</div>
				<?php endif; ?>

				<?php if($blog_type != "normal") : ?>
					<div class="col-md-12 col-sm-12">
				<?php else: ?>
					<div class="col-md-8 col-sm-7 col-xs-12">
				<?php endif; ?>
			<div id="primary" class="content-area" data-blog-type="<?php echo esc_attr($blog_type); ?>">
				<main id="main" class="site-main" role="main">
					<?php if ( have_posts() ) : ?>
						<div class="masonry-holder">
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'inc/post-formats/content', get_post_format() );
							?>

						<?php endwhile; ?>
						</div>

						<?php if($blogoma_paging) : ?>
							<?php blogoma_numeric_paging_nav(); ?>
						<?php else : ?>
							<?php blogoma_paging_nav(); ?>
						<?php endif; ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
			<?php if($layout == "right" && $blog_type == "normal" ): ?>
				<div class="col-md-4 col-sm-5 col-xs-12">
					<?php get_sidebar(); ?>
				</div>
			<?php endif; ?>
		</div>
</div>

<?php get_footer(); ?>
