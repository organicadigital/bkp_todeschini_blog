��    >        S   �      H  
   I     T     j  	   q  9   {  �   �  Q   @     �     �     �  
   �     �     �     �     �     �       	   
  	     	        (  _   <  W   �     �     �                 	     	   (     2  	   H     R  %   c     �  N   �     �     �     �     �     	     	     	     ,	     ;	     I	     P	     ]	     f	     }	     �	  \   �	     �	     �	      
     
     
  	   +
     5
  $   B
     g
  N  m
     �     �     �     �  :   �  �   ,  S   �               %  	   .     8     D     a     n     �     �  
   �     �     �     �  [   �  U   3     �     �     �     �     �     �     �     �     �       "        ;  _   D     �  
   �  	   �     �     �     �     �            
   *     5     =     I     h     u  k   �  
   �          	     #     +  	   3     =  9   L     �     )             /   ,      =   "   1   
                                 :       !   &   (       >   	          9   .   #            2   -                %   *      0   ;          <   6   +   5                     3                         $   8                         4                    7   '       % Comments &larr; Older Comments (Edit) 1 Comment <cite class="fn">%s</cite><span class="says">says:</span> <p class="comment-notes">Please be polite. We appreciate that. Your email address will not be published and required fields are marked</p> <span class="posted-on">Posted by <span class="byline">%2$s</span> on %1$s</span> AND About me Archives Author: %s Categories:  Comment navigation Comments Comments are closed. Continue reading Day: %s Followers Following Galleries I like this article It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Latest Leave a comment Likes Message Meta Month: %s NEXT PAGE Newer Comments &rarr; Next Page Nothing Found... Oops! That page can&rsquo;t be found. PIN IT PRESS <strong>ENTER</strong> TO SEE RESULTS OR <strong>ESC</strong> TO CANCEL. PREVIOUS PAGE Page %s Pages: Photos Please Try Again Popular Post navigation Posted in %1$s Previous Page Random Remove Embed SHARE IT Search Results for: %s Share Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. TWEET IT Tags:  Things you might like Year: %s Your E-mail Your Name Your Website Your comment is awaiting moderation. likes Project-Id-Version: blogoma v.1.0.1
POT-Creation-Date: 2014-06-26 23:54+0200
PO-Revision-Date: 2015-12-15 18:39-0200
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.2
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 % Comentários &larr; Anterior (Editar) 1 Comentario <cite class="fn">%s</cite><span class="says">disse:</span> <p class="comment-notes">Por favor, seja educado. Nós gostamos disso. Seu e-mail não será publicado e os campos obrigatórios estão marcados com "*"</p> <span class="posted-on">Escrito por <span class="byline">%2$s</span> em %1$s</span> E Sobre Arquivos Autor: %s Categorias: Navegação nos Comentários Comentários Comentários estão desativados Leia mais... Dia: %s Seguidores Seguindo Galerias Eu gostei deste artigo Parece que nada foi encontrado por aqui. Tente um dos links abaixo ou que tal uma pesquisa? Parece que não conseguimos encontrar o que você procura. Talvez, pesquisando ajude. Últimos Deixe um comentário Curtidas Mensagem Título da Meta Box Mês: %s PRÓXIMA PÁGINA Próximo &rarr; Próxima Página Nada Encontrado Oops! Página não foi encontrada. PIN ISSO PRESSIONE <strong>ENTER</strong> PARA VER OS RESULTADOS, OU <strong>ESC</strong> PARA CANCELAR. PÁGINA ANTERIOR Página %s Páginas: Fotos Por favor, tente novamente Popular Navegação de Post Publicado em %1$s Página Anterior Aleatório Remover COMPARTILHE Resultados da pesquisa por: %s Compartilhar Pular para o conteúdo Desculpe, nada foi encontrado com os termos de busca. Por favor, tente novamente com outras palavras-chave. TWEET ISSO Tags: Você deve gostar também Ano: %s E-mail* Seu nome* Página da Web Seu comentário está aguardando aprovação do moderador Curtidas 