<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package blogoma
 * Template name: Page - With Sidebar
 * 
 */
$bg 				= get_post_meta($post->ID, 'blogoma_page_bg', true);
$bg_color			= get_post_meta($post->ID, 'blogoma_page_bg_cp', true);
$bg_overlay_color	= get_post_meta($post->ID, 'blogoma_page_bgo_cp', true);
$bg_overlay_alpha	= get_post_meta($post->ID, 'blogoma_page_bgo_alpha', true);

$bg_overlay_alpha 	= $bg_overlay_alpha / 100;

get_header(); ?>

<div class="row" id="page-holder">
	<div class="page-bg"  style="background:<?php echo $bg_color; ?>" >
		<img src="<?php echo $bg; ?>" alt="">
		<div class="page-bg-overlay" style="background:<?php echo $bg_overlay_color;?>; opacity:<?php echo $bg_overlay_alpha;?>"></div>
	</div>
	<div class="container">
		<div class="col-md-8 col-sm-6 col-xs-12">	
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
				<div class="post-paper-bg"></div>
			</div><!-- #primary -->
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
				<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>