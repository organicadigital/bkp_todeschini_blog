<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package blogoma
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Mobile First -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php 
	
	$options = get_option('blogoma_admin'); 

	$site_layout				= $options['site_layout'];
	
	$site_bg_layout				= "";
	$site_bg 					= "";
	$site_pre_bg 				= "";

	if(!empty($options['site_pre_bg_img'])){
		$site_pre_bg = get_template_directory_uri() ."/images/bg-images/". $options['site_pre_bg_img'];
	}
	if(!empty($options['site_bg_img'])){
		$site_bg = $options['site_bg_img'];
	}; 

	$site_bg_repeat_check		= $options['site_bg_img_check'];
	$site_bg_repeat				= $options['site-bg-repeat'];

	
	if(!empty($site_bg_repeat_check)) {
		if(!empty($site_pre_bg)) {
			$site_bg_layout = 'style="background:url('.$site_pre_bg.') 0 0 transparent '.$site_bg_repeat.';"';
		}
		if(!empty($site_bg)) {
			$site_bg_layout = 'style="background:url('.$site_bg.') 0 0 transparent '.$site_bg_repeat.';"';
		}

	}

	if(!empty($options['favicon'])) { 
		echo '<link rel="shortcut icon" href="'. $options['favicon'] .'" />';
	};

?>
<?php wp_head(); ?>
</head>

<body <?php body_class();?> <?php if($site_layout) { echo $site_bg_layout; }; ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=667476666698351";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="page" class="hfeed site <?php if($site_layout) {echo "boxed-page"; } ?>" <?php if(!$site_layout) { echo $site_bg_layout; }; ?> >
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'blogoma' ); ?></a>
			<header id="masthead" class="site-header" role="banner" data-header-sticky="<?php echo $options["header-sticky"]; ?>">
				<div class="row">
					<div class="container">
						<div class="col-md-12">
							<div class="site-branding">
								<?php if(!empty($options['logo-or-text']))  :  ?>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
										<?php if(!empty($options['cs-logo'])) : ?>
										<img src="<?php echo $options['cs-logo']; ?>" alt="<?php echo get_bloginfo( 'name', 'display' ); ?>">
										<?php endif; ?>
									</a>
								<?php else : ?>
									<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
								<?php endif; ?>

							</div>

							<nav id="site-navigation" class="main-navigation hidden-sm hidden-xs" role="navigation">
								<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
							</nav><!-- #site-navigation -->

							<div class="mobile-menu hidden-md hidden-lg">
								<a href="javascript:;">Menu</a>
							</div>
							<nav id="mobile-navigation" class="mobile-navigation hidden-md hidden-lg" role="mobile-navigation">
								<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
							</nav>
							<?php 
									if(!empty($options['search'])) :
							?>
							<div class="site-search hidden-sm  hidden-xs">
								<a href="javascript:;" class="search-btn">Search</a>
							</div>
							<div class="search-holder">
								<?php get_search_form(); ?>
								<span class="search-info">
									<?php echo __('PRESS <strong>ENTER</strong> TO SEE RESULTS OR <strong>ESC</strong> TO CANCEL.', 'blogoma'); ?>
								</span>
								<a href="javascript:;" class="search-btn close-btn">Close</a>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</header><!-- #masthead -->
	<div id="content" class="site-content">
