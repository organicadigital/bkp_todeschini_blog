<?php
/**
 * The Template for displaying all single posts.
 *
 * @package blogoma
 */

get_header();

$options 			= get_option('blogoma_admin'); 
$layout				= $options['blog_layout'];

?>

<div class="row">
	<div class="container">
		<?php if($layout == "left"): ?>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<?php get_sidebar(); ?>
			</div>
		<?php endif; ?>
		<?php if($layout == "fullwidth"): ?>
			<div class="col-md-12 col-sm-12">
		<?php else: ?>
			<div class="col-md-8 col-sm-7 col-xs-12">
		<?php endif; ?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'inc/post-formats/content', get_post_format() ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() ) :
							comments_template();
						endif;
					?>

				<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<?php if($layout == "right"): ?>
			<div class="col-md-4 col-sm-5 col-xs-12">
				<?php get_sidebar(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>