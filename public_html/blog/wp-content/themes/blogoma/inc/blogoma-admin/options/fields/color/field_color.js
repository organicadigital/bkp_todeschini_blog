jQuery(document).ready(function ($) {
    $('input.popup-colorpicker').wpColorPicker({
    	palettes: ['#1C98E2', '#00BD9C', '#0AD169', '#F4C600', '#E88000', '#9C51B6','#ED4A29', '#CE537E']
    });
});
