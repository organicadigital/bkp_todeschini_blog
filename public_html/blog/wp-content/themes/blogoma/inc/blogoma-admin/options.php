<?php
/*
 *
 * Set the text domain for the theme or plugin.
 *
 */
define('Redux_TEXT_DOMAIN', 'blogoma_admin');

/*
 *
 * Require the framework class before doing anything else, so we can use the defined URLs and directories.
 * If you are running on Windows you may have URL problems which can be fixed by defining the framework url first.
 *
 */
//define('Redux_OPTIONS_URL', site_url('path the options folder'));

if(!class_exists('Redux_Options')){
    require_once(dirname(__FILE__) . '/options/defaults.php');
}

/*
 *
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
    //$sections = array();
    $sections[] = array(
        'title' => __('A Section added by hook', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', Redux_TEXT_DOMAIN),
		'icon' => 'paper-clip',
		'icon_class' => 'icon-large',
        // Leave this as a blank section, no options just some intro text set above.
        'fields' => array()
    );

    return $sections;
}

//add_filter('redux-opts-sections-twenty_eleven', 'add_another_section');


/*
 * 
 * Custom function for filtering the args array given by a theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
    //$args['dev_mode'] = false;
    
    return $args;
}
//add_filter('redux-opts-args-twenty_eleven', 'change_framework_args');


/*
 *
 * Most of your editing will be done in this section.
 *
 * Here you can override default values, uncomment args and change their values.
 * No $args are required, but they can be over ridden if needed.
 *
 */
function setup_framework_options(){
    $args = array();

    // Setting dev mode to true allows you to view the class settings/info in the panel.
    // Default: true
    $args['dev_mode'] = false;

	// Set the icon for the dev mode tab.
	// If $args['icon_type'] = 'image', this should be the path to the icon.
	// If $args['icon_type'] = 'iconfont', this should be the icon name.
	// Default: info-sign
	//$args['dev_mode_icon'] = 'info-sign';

	// Set the class for the dev mode tab icon.
	// This is ignored unless $args['icon_type'] = 'iconfont'
	// Default: null
	$args['dev_mode_icon_class'] = 'icon-large';

    // If you want to use Google Webfonts, you MUST define the api key.
    //$args['google_api_key'] = 'xxxx';

    // Define the starting tab for the option panel.
    // Default: '0';
    //$args['last_tab'] = '0';

    // Define the option panel stylesheet. Options are 'standard', 'custom', and 'none'
    // If only minor tweaks are needed, set to 'custom' and override the necessary styles through the included custom.css stylesheet.
    // If replacing the stylesheet, set to 'none' and don't forget to enqueue another stylesheet!
    // Default: 'standard'
    //$args['admin_stylesheet'] = 'standard';

    // Add HTML before the form.
    //$args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Add content after the form.
    //$args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Set footer/credit line.
    //$args['footer_credit'] = __('<p>This text is displayed in the options panel footer across from the WordPress version (where it normally says \'Thank you for creating with WordPress\'). This field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Setup custom links in the footer for share icons
    //$args['share_icons']['twitter'] = array(
    //    'link' => 'http://twitter.com/themeticaWP',
    //    'title' => 'Follow me on Twitter', 
    //    'img' => Redux_OPTIONS_URL . 'img/social/Twitter.png'
	// );
    //$args['share_icons']['linked_in'] = array(
    //    'link' => 'http://www.linkedin.com/profile/view?id=52559281',
    //    'title' => 'Find me on LinkedIn', 
    //    'img' => Redux_OPTIONS_URL . 'img/social/LinkedIn.png'
    //);

    // Enable the import/export feature.
    //Default: true
    //$args['show_import_export'] = false;

	// Set the icon for the import/export tab.
	// If $args['icon_type'] = 'image', this should be the path to the icon.
	// If $args['icon_type'] = 'iconfont', this should be the icon name.
	// Default: refresh
	$args['import_icon'] = 'download';

	// Set the class for the import/export tab icon.
	// This is ignored unless $args['icon_type'] = 'iconfont'
	// Default: null
	$args['import_icon_class'] = 'icon-large';

    // Set a custom option name. Don't forget to replace spaces with underscores!
    $args['opt_name'] = 'blogoma_admin';

    // Set a custom menu icon.
    //$args['menu_icon'] = '';

    // Set a custom title for the options page.
    // Default: Options
    $args['menu_title'] = __('Themetica Options', Redux_TEXT_DOMAIN);

    // Set a custom page title for the options page.
    // Default: Options
    $args['page_title'] = __('', Redux_TEXT_DOMAIN);

    // Set a custom page slug for options page (wp-admin/themes.php?page=***).
    // Default: redux_options
    $args['page_slug'] = 'redux_options';

    // Set a custom page capability.
    // Default: manage_options
    //$args['page_cap'] = 'manage_options';

    // Set the menu type. Set to "menu" for a top level menu, or "submenu" to add below an existing item.
    // Default: menu
    //$args['page_type'] = 'submenu';

    // Set the parent menu.
    // Default: themes.php
    // A list of available parent menus is available at http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    //$args['page_parent'] = 'options_general.php';

    // Set a custom page location. This allows you to place your menu where you want in the menu order.
    // Must be unique or it will override other items!
    // Default: null
    $args['page_position'] = 50;

    // Set a custom page icon class (used to override the page icon next to heading)
    //$args['page_icon'] = 'icon-themes';

	// Set the icon type. Set to "iconfont" for Font Awesome, or "image" for traditional.
	// Redux no longer ships with standard icons!
	// Default: iconfont
	//$args['icon_type'] = 'image';

    // Disable the panel sections showing as submenu items.
    // Default: true
    //$args['allow_sub_menu'] = false;
        
    // Set ANY custom page help tabs, displayed using the new help tab API. Tabs are shown in order of definition.
    $args['help_tabs'][] = array(
        'id' => 'redux-opts-1',
        'title' => __('Theme Information 1', Redux_TEXT_DOMAIN),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', Redux_TEXT_DOMAIN)
    );
    $args['help_tabs'][] = array(
        'id' => 'redux-opts-2',
        'title' => __('Theme Information 2', Redux_TEXT_DOMAIN),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', Redux_TEXT_DOMAIN)
    );

    // Set the help sidebar for the options page.                                        
    $args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', Redux_TEXT_DOMAIN);

    $sections = array();
	/*
    $sections[] = array(
		// Redux uses the Font Awesome iconfont to supply its default icons.
		// If $args['icon_type'] = 'iconfont', this should be the icon name minus 'icon-'.
		// If $args['icon_type'] = 'image', this should be the path to the icon.
		'icon' => 'paper-clip',
		// Set the class for this icon.
		// This field is ignored unless $args['icon_type'] = 'iconfont'
		'icon_class' => 'icon-large',
        'title' => __('Getting Started', Redux_TEXT_DOMAIN),
		'desc' => __('<p class="description">This is the description field for this section. HTML is allowed</p>', Redux_TEXT_DOMAIN),
        // Lets leave this as a blank section, no options just some intro text set above.
        //'fields' => array()
    );
	*/

    $sections[] = array(
		'icon' => 'home',
		'icon_class' => 'icon-large',
        'title' => __('General Options', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description">Welcome to the blogoma Admin Panel!</p>', Redux_TEXT_DOMAIN),
        'fields' => array(
			array(
                'id' => 'favicon',
                'type' => 'upload',
                'title' => __('Favico Upload', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Upload a 16px x 16px .png or .gif ', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN)
            ),
            array(
                'id' => 'blogoma_paging',
                'type' => 'checkbox',
                'title' => __('Show numeric paging', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to show numeric paging on blog posts, it should be open.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
            array(
                'id' => 'retina',
                'type' => 'checkbox',
                'title' => __('Retina Support?', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to use retina support for your image. it should be open.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0' // 1 = checked | 0 = unchecked
            ),
			array(
                'id' => 'google_analytics',
                'type' => 'textarea',
                'title' => __('Google Analytics', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Please enter in your google analytics tracking code here without "script" tags.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN)
            ),
            array(
                'id' => 'custom_css',
                'type' => 'textarea',
                'title' => __('Custom CSS', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you have any custom CSS you would like added to the site, please enter it here without "style" tags.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'validate' => 'html'
            )
        )
    );

    $sections[] = array(
        'icon' => 'home',
        'icon_class' => 'icon-large',
        'title' => __('Theme Color Options', Redux_TEXT_DOMAIN),
        'desc' => __('', Redux_TEXT_DOMAIN),
        'fields' => array(
            array(
                'id' => 'theme_color',
                'type' => 'color',
                'title' => __('Main Color', Redux_TEXT_DOMAIN), 
                'sub_desc' => 'Change this color to alter the accent color globally for your site. If you\'re stuck, try one of the eight pre-picked colors that are guaranteed to look awesome!',
                'desc' => '',
                'std' => '#ed4a29'
            ),
            array(
                'id' => 'site_header_color',
                'type' => 'color',
                'title' => __('Site Header Background Color', Redux_TEXT_DOMAIN), 
                'sub_desc' => 'Change this color if you want to change site header background color',
                'desc' => '',
                'std' => '#ffffff'
            ),
            array(
                'id' => 'site_bg_color',
                'type' => 'color',
                'title' => __('Site Background Color', Redux_TEXT_DOMAIN), 
                'sub_desc' => 'Change this color if you want to change site background color',
                'desc' => '',
                'std' => '#EEEEEE'
            ),
            array(
                'id' => 'footer_bg_color',
                'type' => 'color',
                'title' => __('Site Footer Background Color', Redux_TEXT_DOMAIN), 
                'sub_desc' => 'Change this color if you want to change site footer background color',
                'desc' => '',
                'std' => '#444444'
            ),
        )
    );


    $sections[] = array(
        'icon' => 'home',
        'icon_class' => 'icon-large',
        'title' => __('Header Options', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(
            array(
                'id' => 'logo-or-text',
                'type' => 'checkbox_hide_below',
                'title' => __('Blogoma Logo Option', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If left unchecked, plain text will be used instead (generated from site name).', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std'=>'0',
                'next_to_hide' => '5'
            ),
            array(
                'id' => 'cs-logo',
                'type' => 'upload',
                'title' => __('Logo Upload', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Upload your logo here.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'std' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/blogoma-logo.png'
            ),
            array(
                'id'       => 'logo-padding',
                'type'     => 'text',
                'title'    => __('Logo padding values'),
                'sub_desc' => __('please, give padding value for logo <br> default value is; 24px 50px 24px 0px<br>order is; top, right, bottom, left', Redux_TEXT_DOMAIN),
                'desc'     => __('', Redux_TEXT_DOMAIN),
                'std'      => '24px 50px 24px 0px'
            ),
            array(
                'id'       => 'menu-item-padding',
                'type'     => 'text',
                'title'    => __('Menu item padding values'),
                'sub_desc' => __('please, give padding value for menu item <br> default value is; 38px 0px 34px 0px<br>order is; top, right, bottom, left', Redux_TEXT_DOMAIN),
                'desc'     => __('', Redux_TEXT_DOMAIN),
                'std'      => '38px 0px 34px 0px'
            ),
            array(
                'id'       => 'search-item-padding',
                'type'     => 'text',
                'title'    => __('Search icon padding values'),
                'sub_desc' => __('please, give padding value for search icon <br> default value is; 30px 0px 30px 0px<br>order is; top, right, bottom, left', Redux_TEXT_DOMAIN),
                'desc'     => __('', Redux_TEXT_DOMAIN),
                'std'      => '30px 0px 30px 0px'
            ),
            array(
                'id' => 'header-sticky',
                'type' => 'checkbox',
                'title' => __('Header Sticky', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to sticky header, it should be open.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
            array(
                'id' => 'search',
                'type' => 'checkbox',
                'title' => __('Show Search', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to show search bar on the header, it should be open.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            )
        )
    );
	
	$sections[] = array(
		'icon' => 'list-alt',
		'icon_class' => 'icon-large',
        'title' => __('Layout Options', Redux_TEXT_DOMAIN),
        'desc' => __('You can change website layout and body background pattern or image options...', Redux_TEXT_DOMAIN),
        'fields' => array(
           array(
                'id' => 'site_layout',
                'type' => 'checkbox',
                'title' => __('Boxed Layout?', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to boxed layout, it should be open.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0' , // 1 = checked | 0 = unchecked
            ),
			array(
                'id' => 'site_bg_img_check',
                'type' => 'checkbox_hide_below',
                'title' => __('Do you want to use pattern image on backrgound?', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to use pattern image, it should be open. * it will overwrite solid color options', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0' , // 1 = checked | 0 = unchecked
				'next_to_hide' => '3'
            ),
            array(
                'id' => 'site_pre_bg_img',
                'type' => 'radio_img',
                'title' => __('Pre-defined site background image', BLOGOMA_THEME_NAME), 
                'sub_desc' => __('Please select an option you would like for your blog.', BLOGOMA_THEME_NAME),
                'desc' => __('', BLOGOMA_THEME_NAME),
                'options' => array(
                                'scribble_light.png' => array('title' => 'Bg-1', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/scribble_light.png'),
                                'gplaypattern.png' => array('title' => 'Bg-2', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/gplaypattern.png'),
                                'tex2.png' => array('title' => 'Bg-3', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/tex2.png'),
                                'hexellence.png' => array('title' => 'Bg-4', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/hexellence.png'),
                                'rough_diagonal.png' => array('title' => 'Bg-5', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/rough_diagonal.png'),
                                'old_wall.png' => array('title' => 'Bg-6', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/old_wall.png'),
                                'noise_lines.png' => array('title' => 'Bg-7', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/noise_lines.png'),
                                'vichy.png' => array('title' => 'Bg-8', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/vichy.png'),
                                'double_lined.png' => array('title' => 'Bg-9', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/double_lined.png'),
                                'smooth_wall.png' => array('title' => 'Bg-10', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/smooth_wall.png'),
                            ),
                'std' => 'scribble_light.png'
            ), 
			array(
                'id' => 'site_bg_img',
                'type' => 'upload',
                'title' => __('Site Background Image Upload', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Please upload an image that will be used body background image.', Redux_TEXT_DOMAIN),
                'desc' => ''
            ),
			array(
                'id' => 'site-bg-repeat',
                'type' => 'select',
                'title' => __('Site Background Repeat Options', Redux_TEXT_DOMAIN), 
                'sub_desc' => "",
                'desc' => "",
				'options' => Array('repeat'=>"All", 'repeat-x' => "Repeat-X", 'repeat-y'=>"Repeat-Y"),
				'std'=>'all'
            ), 
        )
    );
  	
	/* Custom FONT Settings  */
	global $google_fonts;
	global $os_fonts;
	global $font_sizes;
	
	/*
    $sections[] = array(
		'icon' => 'font',
		'icon_class' => 'icon-large',
        'title' => __('Typography Options', Redux_TEXT_DOMAIN),
        'desc' => __('You can choose standart or google fonts.', Redux_TEXT_DOMAIN),
        'fields' => array(
            array(
                'id' => 'custom_fonts',
                'type' => 'checkbox_hide_below',
                'title' => __('Use Google fonts?', Redux_TEXT_DOMAIN),
                'sub_desc' => __('See previews of all these fonts at <a target="_blank" href="http://www.google.com/fonts/">Google Web Fonts</a>', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0',
				'next_to_hide' => '6'
            ),
			array(
                'id' => 'main_font',
                'type' => 'select',
                'title' => __('Main font', Redux_TEXT_DOMAIN), 
                'sub_desc' => "",
                'desc' => "",
                'desc' => "",
				'options' => $google_fonts
            ),   
            array(
                'id' => 'second_font',
                'type' => 'select',
                'title' => __('Secondary font', Redux_TEXT_DOMAIN), 
                'sub_desc' => "",
                'desc' => "",
                'desc' => "",
                'options' => $google_fonts
            ),  		
			array(
                'id' => 'heading_font',
                'type' => 'select',
                'title' => __('Headings', Redux_TEXT_DOMAIN), 
                'sub_desc' => 'h1, h2, h3, h4, h5, h6',
                'desc' => "",
				'options' => $google_fonts
            ),	
            array(
                'id' => 'quote_font',
                'type' => 'select',
                'title' => __('Link and Quote post type font', Redux_TEXT_DOMAIN), 
                'sub_desc' => '',
                'desc' => "",
                'options' => $google_fonts
            ),						
			array(
                'id' => 'code_font',
                'type' => 'select',
                'title' => __('Code font', Redux_TEXT_DOMAIN), 
                'sub_desc' => "",
                'desc' => "",
				'options' => $google_fonts
            ),
			array(
                'id' => 'include_all',
                'type' => 'checkbox',
                'title' => __('Include All Characters', Redux_TEXT_DOMAIN), 
                'sub_desc' => 'latin,latin-ext,vietnamese,cyrillic,greek,greek-ext,cyrillic-ext',
                'desc' => '',
				'switch' => true,
                'std' => '0' 
            ),			
			array(
                'id' => 'custom_typo',
                'type' => 'checkbox_hide_below',
                'title' => __('Use custom font size?', Redux_TEXT_DOMAIN),
                'sub_desc' => __('If you want, you could set your font size...', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0',
				'next_to_hide' => '9'
            ),
			array(
                'id' => 'body_font_size', 
                'type' => 'select', 
                'title' => __('Body', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Default 18px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '4'
			),
			array(
                'id' => 'page_title_font_size', 
                'type' => 'select', 
                'title' => __('Page/Post Title', Redux_TEXT_DOMAIN),
				'sub_desc' => __('Default 50px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '41'
			),
            array(
                'id' => 'quote_title_font_size', 
                'type' => 'select', 
                'title' => __('Link and Quote post type font size', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Default 50px', Redux_TEXT_DOMAIN),
                'desc' => '',
                'options' => $font_sizes,
                'std' => '41'
            ),
			array(
                'id' => 'h1_font_size', 
                'type' => 'select', 
                'title' => __('H1', Redux_TEXT_DOMAIN),
				'sub_desc' => __('Default 50px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '41'
			),
			array(
                'id' => 'h2_font_size', 
                'type' => 'select', 
                'title' => __('H2', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Default 35px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '26'
			),
			array(
                'id' => 'h3_font_size', 
                'type' => 'select', 
                'title' => __('H3', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Default 22px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '13'
			),
			array(
                'id' => 'h4_font_size', 
                'type' => 'select', 
                'title' => __('H4', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Default 18px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '9'
			),
			array(
                'id' => 'h5_font_size', 
                'type' => 'select', 
                'title' => __('H5', Redux_TEXT_DOMAIN),
				'sub_desc' => __('Default 15px', Redux_TEXT_DOMAIN),
				'desc' => '',
                'options' => $font_sizes,
				'std' => '6'
			),
            array(
                'id' => 'code_font_size', 
                'type' => 'select', 
                'title' => __('Code Quote', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Default 11px', Redux_TEXT_DOMAIN),
                'desc' => '',
                'options' => $font_sizes,
                'std' => '2'
            ),
        )
    );
    
	*/
    $sections[] = array(
        'icon' => 'home',
        'icon_class' => 'icon-large',
        'title' => __('Featured Post Options', Redux_TEXT_DOMAIN),
        'desc' => __('Do you want to use featured post on homepage?', Redux_TEXT_DOMAIN),
        'fields' => array(
            array(
                'id' => 'featured_post',
                'type' => 'checkbox',
                'title' => __('', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('If you want to show featured posts on homepage. it should be open.', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0' // 1 = checked | 0 = unchecked
            ),
            array(
                'id' => 'featured_type',
                'type' => 'select',
                'title' => __('Featured Post Type', Redux_TEXT_DOMAIN), 
                'sub_desc' => "",
                'desc' => "",
                'options' => Array('2' => '2 Column', '2+3'=>'2+3 Column', '3'=>"3 Column", '3+2' => "3+2 Column", "3+3" => '3+3 Column', 'slide'=>"Slider"),
                'std'=>'all'
            ), 
        )
    );



	$sections[] = array(
		'icon' => 'pencil-square-o',
		'icon_class' => 'icon-large',
        'title' => __('Blog Options', Redux_TEXT_DOMAIN),
        'desc' => __('You can change your blog view options...', Redux_TEXT_DOMAIN),
        'fields' => array(
            array(
				'id' => 'blog_layout',
				'type' => 'radio_img',
				'title' => __('Blog Sidebar', BLOGOMA_THEME_NAME), 
				'sub_desc' => __('Please select an option you would like for your blog.', BLOGOMA_THEME_NAME),
				'desc' => __('', BLOGOMA_THEME_NAME),
				'options' => array(
								'fullwidth' => array('title' => 'Full Width (No Sidebar)', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/1col.png'),
								'right' => array('title' => 'Right Sidebar', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/2cr.png'),								
								'left' => array('title' => 'Left Sidebar', 'img' => BLOGOMA_FRAMEWORK_DIRECTORY.'/options/img/2cl.png'),
							),
				'std' => 'right'
			),	
            
            array(
                'id' => 'readmore_show',
                'type' => 'checkbox',
                'title' => __('Show Continue Reading Button', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want show continue reading button on your post?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
			array(
                'id' => 'author_show',
                'type' => 'checkbox',
                'title' => __('Show Author', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want use author info box in your post detail?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
			array(
                'id' => 'tags_show',
                'type' => 'checkbox',
                'title' => __('Show Tags ', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want use tags box in your post detail?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),

            array(
                'id' => 'related_content',
                'type' => 'checkbox',
                'title' => __('Show Related Content Box ', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want show related content box above the post?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
            array(
                'id' => 'show_like',
                'type' => 'checkbox',
                'title' => __('Show Post Like Buttons', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want use post like option in your post?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
			array(
                'id' => 'show_share',
                'type' => 'checkbox',
                'title' => __('Show Social Sharing Buttons ', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want use social sharing buttons in your post detail?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
            
        )
    );
	
	$sections[] = array(
		'icon' => 'tasks',
		'icon_class' => 'icon-large',
        'title' => __('Footer Options', Redux_TEXT_DOMAIN),
        'desc' => __('You can set footer options... ', Redux_TEXT_DOMAIN),
        'fields' => array(
			array(
                'id' => 'footer_show',
                'type' => 'checkbox',
                'title' => __('Show Footer', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want to show footer?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1', // 1 = checked | 0 = unchecked
            ),
            array(
                'id' => 'social_media_show',
                'type' => 'checkbox_hide_below',
                'title' => __('Show Social Media Accounts', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want show your social media account link on footer?', Redux_TEXT_DOMAIN),
                'switch' => true,
                'desc' =>'',
                'std' => '1', // 1 = checked | 0 = unchecked
                'next_to_hide' => '11'
            ),
            array(
                'id' => 'facebook',
                'type' => 'text',
                'title' => __('Facebook', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.facebook.com/username ', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'twitter',
                'type' => 'text',
                'title' => __('Twitter', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.twitter.com/username ', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'behance',
                'type' => 'text',
                'title' => __('Behance', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.behance.com/username ', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'dribbble',
                'type' => 'text',
                'title' => __('Dribbble', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.dribbble.com/username ', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'instagram',
                'type' => 'text',
                'title' => __('Instagram', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.instagram.com/username ', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'pinterest',
                'type' => 'text',
                'title' => __('Pinterest', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.pinterest.com/username ', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'google',
                'type' => 'text',
                'title' => __('Google+', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://plus.google.com/username', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'youtube',
                'type' => 'text',
                'title' => __('Youtube', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.youtube.com/user/username', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'flickr',
                'type' => 'text',
                'title' => __('Flickr', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.flickr.com/username', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'soundcloud',
                'type' => 'text',
                'title' => __('Soundcloud', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.soundcloud.com/username', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'linkedin',
                'type' => 'text',
                'title' => __('Linkedin', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; https://www.linkedin.com/username', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
            array(
                'id' => 'rss',
                'type' => 'text',
                'title' => __('RSS', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('ie; http://www.yoursite.com/feed/', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std'=> ''
            ),
			array(
                'id' => 'copyright_show',
                'type' => 'checkbox_hide_below',
                'title' => __('Show Copyright', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want to show copy right area on footer?', Redux_TEXT_DOMAIN),
				'switch' => true,
                'desc' =>'',
				'std' => '1' // 1 = checked | 0 = unchecked
            ),
			 array(
                'id' => 'copyright_text',
                'type' => 'textarea',
                'title' => __('Copyright Text', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Please enter the copyright section text. e.g. All Rights Reserved, Themetica Themes.', Redux_TEXT_DOMAIN),
                'validate' => 'html',
				'std'=>'Blogoma is a personal blog theme with a clean & flat design.<br><a href="http://www.wordpress.com">Powered by Wordpress.</a> <a href="http://www.themetica.com">Designed by Themetica™</a>'
            )
        )
    );
	
	$sections[] = array(
		'icon' => 'map-marker',
		'icon_class' => 'icon-large',
        'title' => __('Google Map Options', Redux_TEXT_DOMAIN),
        'desc' => __('If you want to use this option. You should select a contact page template name as "Contact - With Google Map" <br>To convert an address into latitude & longitude please use <a href="http://www.latlong.net/convert-address-to-lat-long.html">this converter.</a>', Redux_TEXT_DOMAIN),
        'fields' => array(
			array(
                'id' => 'use-google-map',
                'type' => 'checkbox_hide_below',
                'title' => __('Google Maps', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want use google maps on your contact page?', Redux_TEXT_DOMAIN),
                'desc' => __('', Redux_TEXT_DOMAIN),
                'std' => '1' ,
				'switch' => true,
				'next_to_hide' => '7'
            ),
			array(
                'id' => 'latitude',
                'type' => 'text',
                'title' => __('Latitude', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Please enter the latitude for your first location.', Redux_TEXT_DOMAIN),
                'desc' => '',
                'validate' => 'numeric',
				'std'=>40.714623
            ),
            array(
                'id' => 'longitude',
                'type' => 'text',
                'title' => __('Longitude', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Please enter the longitude for your first location.', Redux_TEXT_DOMAIN),
                'desc' => '',
                'validate' => 'numeric',
				'std'=>-74.006605
            ),
			array(
                'id' => 'marker-img',
                'type' => 'upload',
                'title' => __('Marker Icon Upload', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Please upload an image that will be used for all the markers on your map.', Redux_TEXT_DOMAIN),
                'desc' => ''
            ),
			array(
                'id' => 'zoom-level',
                'type' => 'select',
                'title' => __('Default Map Zoom Level', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Value should be between 1-18', Redux_TEXT_DOMAIN),
				'options' => array("1"=>1, "2"=>2, "3"=>3, "4"=>4, "5"=>5, "6"=>6, "7"=>7, "8"=>8, "9"=>9, "10"=>10, "11"=>11, "12"=>12, "13"=>13, "14"=>14, "15"=>15, "16"=>16, "17"=>17, "18"=>18), 
                'std' => '1'
            ),
			array(
                'id' => 'enable-zoom',
                'type' => 'checkbox',
                'title' => __('Enable Map Zoom In/Out', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Do you want users to be able to zoom in/out on the map?', Redux_TEXT_DOMAIN),
                'desc' => '',
				'switch' => true,
                'std' => '0' 
            ),
			array(
                'id' => 'enable-animation',
                'type' => 'checkbox',
                'title' => __('Enable Animation', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Your markers to do a quick bounce as they load in.', Redux_TEXT_DOMAIN),
                'desc' => '',
                'std' => '1' ,
				'switch' => true,
            ),
        )
    );
	
                
    $tabs = array();

    if (function_exists('wp_get_theme')){
        $theme_data = wp_get_theme();
        $item_uri = $theme_data->get('ThemeURI');
        $description = $theme_data->get('Description');
        $author = $theme_data->get('Author');
        $author_uri = $theme_data->get('AuthorURI');
        $version = $theme_data->get('Version');
        $tags = $theme_data->get('Tags');
    }else{
        $theme_data = get_theme_data(trailingslashit(get_stylesheet_directory()) . 'style.css');
        $item_uri = $theme_data['URI'];
        $description = $theme_data['Description'];
        $author = $theme_data['Author'];
        $author_uri = $theme_data['AuthorURI'];
        $version = $theme_data['Version'];
        $tags = $theme_data['Tags'];
     }
    
    $item_info = '<div class="redux-opts-section-desc">';
    $item_info .= '<p class="redux-opts-item-data description item-uri">' . __('<strong>Theme URL:</strong> ', Redux_TEXT_DOMAIN) . '<a href="' . $item_uri . '" target="_blank">' . $item_uri . '</a></p>';
    $item_info .= '<p class="redux-opts-item-data description item-author">' . __('<strong>Author:</strong> ', Redux_TEXT_DOMAIN) . ($author_uri ? '<a href="' . $author_uri . '" target="_blank">' . $author . '</a>' : $author) . '</p>';
    $item_info .= '<p class="redux-opts-item-data description item-version">' . __('<strong>Version:</strong> ', Redux_TEXT_DOMAIN) . $version . '</p>';
    $item_info .= '<p class="redux-opts-item-data description item-description">' . $description . '</p>';
    $item_info .= '<p class="redux-opts-item-data description item-tags">' . __('<strong>Tags:</strong> ', Redux_TEXT_DOMAIN) . implode(', ', $tags) . '</p>';
    $item_info .= '</div>';

    $tabs['item_info'] = array(
		'icon' => 'asterisk',
		'icon_class' => 'icon-large',
        'title' => __('Theme Information', Redux_TEXT_DOMAIN),
        'content' => $item_info
    );
    
    if(file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
        $tabs['docs'] = array(
			'icon' => 'book',
			'icon_class' => 'icon-large',
            'title' => __('Documentation', Redux_TEXT_DOMAIN),
            'content' => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
        );
    }

    global $Redux_Options;
    $Redux_Options = new Redux_Options($sections, $args, $tabs);

}
add_action('init', 'setup_framework_options', 0);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value) {
    print_r($field);
    print_r($value);
}

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value) {
    $error = false;
    $value =  'just testing';
    /*
    do your validation
    
    if(something) {
        $value = $value;
    } elseif(somthing else) {
        $error = true;
        $value = $existing_value;
        $field['msg'] = 'your custom error message';
    }
    */
    
    $return['value'] = $value;
    if($error == true) {
        $return['error'] = $field;
    }
    return $return;
}
