<div class="row">
	<div class="container">
		<div class="col-md-12">

<?php

$options 			= get_option('blogoma_admin'); 
$featured_column	= $options['featured_type'];
$post_count 	= 0;
$type 			= "";


switch ($featured_column) {
case "2":
    $post_count = 2;
    $type 		= "half";
    break;
case "2+3":
    $post_count = 5;
    $type 		= "half";
    break;
case "3":
    $post_count = 3;
    $type 		= "third";
    break;
case "3+2":
    $post_count = 5;
    $type 		= "third";
    break;
case "3+3":
    $post_count = 6;
    $type 		= "third";
    break;
case "slide":
    $post_count = 6;
    $type 		= "full";
    break;
}

$args = array(
	'post_type' => 'post',
	'posts_per_page' => -1,
	'order' => 'DESC'
);
// The Query
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {

	$featured_count = 0;
	
	if($featured_column == "slide"){
			echo '<div class="featured-slider-holder"><div class="featured-slider flexslider"><ul class="slides">';
	}

	while ( $the_query->have_posts() ) {

		$the_query->the_post();
		
		$featured			= get_post_meta( get_the_ID(), 'blogoma_featured_checkbox', true );	

		if($featured_column == "slide"){
			if($featured == "on"){
				echo "<li>";

				if (has_post_thumbnail( $post->ID ) ){
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-full' ); 
					        $featured_img = '
					        	<img src="'.$image[0] .'" alt="'.get_the_title().'">';
					    		echo $featured_img;
					    }
				echo '<div class="flex-caption">';
				the_title(sprintf( '<h3 class="featured-entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
				
				echo '<div class="slider-post-data">';
					//echo '<li>' . get_the_title() . '</li>';
				    echo '<div class="featured-meta entry-meta">';
					
					blogoma_posted_on();
					
					/* translators: used between list items, there is a space after the comma */
					$categories_list = get_the_category_list( __( ', ', 'blogoma' ) );

					if ( $categories_list && blogoma_categorized_blog() ) :
						echo '<span class="cat-links"> ';
						printf(__( 'in %1$s', 'blogoma' ), $categories_list );
						echo '</span>';
					endif;

				echo '</div></li>';
			}
		}else{
			if($featured_count < $post_count){

				if($featured == "on"){
					$featured_count++;


					if($featured_column == "2+3"){
						if($featured_count > 2){
							$type = "third";	
						}
					}

					if($featured_column == "3+2"){
						if($featured_count > 3){
							$type = "half";	
						}
					}

					if($featured_column == "3+2" && $featured_count == 3 || $featured_column == "3+3" && $featured_count == 3){
						echo '<div class="featured-item featured-'.$type.' featured-last">';	
					}else{
						echo '<div class="featured-item featured-'.$type.'">';
					}

					

					if (has_post_thumbnail( $post->ID ) ){
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-'.$type.'' ); 
				        $featured_img = '
				        	<div class="image-holder">
				        	<img src="'.$image[0] .'" alt="'.get_the_title().'">
				    		</div>';
				    		echo $featured_img;
				    }

				    echo '<div class="post-data">';
				    the_title( sprintf( '<h3 class="featured-entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );

					//echo '<li>' . get_the_title() . '</li>';
				    echo '<div class="featured-meta entry-meta">';
					
					$date = '<span class="entry-date">'.get_the_date().'</span>';
					echo $date;
					
					/* translators: used between list items, there is a space after the comma */
					$categories_list = get_the_category_list( __( ', ', 'blogoma' ) );

					if ( $categories_list && blogoma_categorized_blog() ) :
						echo '<span class="cat-links"> ';
						printf(__( 'in %1$s', 'blogoma' ), $categories_list );
						echo '</span>';
					endif;

					echo '</div>';
					echo '</div>';
					echo '</div>';

				}
			}
		}

	}
	if($featured_column == "slide"){
				echo '</div></div></ul>';
		}
} else {
	if($featured_count == 0){
		// no posts found
		echo '<h1>Please, select featured post on post detail page.</h1>';
	}
}
/* Restore original Post Data */
wp_reset_postdata();
?>


		</div>
	</div>
</div>