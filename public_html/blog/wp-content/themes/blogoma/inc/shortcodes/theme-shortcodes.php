<?php 
/*
	Blogoma Shortcodes
*/

/* ==========================================================================
Dropcaps
========================================================================== */

function add_blogoma_dropcap( $atts, $content = null ) {
  extract(shortcode_atts(array("style" => '', "color"=>''), $atts)); 
  return '<span class="dropcap '.$style.' " style="color:'.$color.'">' .  $content . '</span>';
} 

add_shortcode('blogoma_dropcap', 'add_blogoma_dropcap');

/* ==========================================================================
Columns
========================================================================== */

function add_blogoma_column( $atts, $content = null ) {
  extract(shortcode_atts(array("column_type" => '', "last_item"=>''), $atts)); 
  
  switch($column_type){
  	case '1/2' :
  		if($last_item != "yes"){
  			$html = '<div class="one_half">'.  $content .'</div>';
  		}
  		else{
  			$html = '<div class="one_half last_item">'.  $content .'</div>';
  		}
  		
		return $html;
	break;
	case '1/3' :
		if($last_item != "yes"){
  			$html = '<div class="one_third">'.  $content .'</div>';
  		}
  		else{
  			$html = '<div class="one_third last_item">'.  $content .'</div>';
  		}
  		
		return $html;
	break;
	case '2/3' :
		if($last_item != "yes"){
  			$html = '<div class="two_third">'.  $content .'</div>';
  		}
  		else{
  			$html = '<div class="two_third last_item">'.  $content .'</div>';
  		}
  		
		return $html;
	break;
	case '1/4' :
		if($last_item != "yes"){
  			$html = '<div class="one_fourth">'.  $content .'</div>';
  		}
  		else{
  			$html = '<div class="one_fourth last_item">'.  $content .'</div>';
  		}
  		
		return $html;
	break;
	case '3/4' :
		if($last_item != "yes"){
  			$html = '<div class="three_fourth">'.  $content .'</div>';
  		}
  		else{
  			$html = '<div class="three_fourth last_item">'.  $content .'</div>';
  		}
  		
		return $html;
	break;
  }

} 

add_shortcode('blogoma_column', 'add_blogoma_column');

/* ==========================================================================
Title
========================================================================== */

function add_blogoma_title( $atts, $content = null ) {
  extract(shortcode_atts(array("type" => ''), $atts)); 

   switch($type){
	  	case 'h1' :
			return '<h1>'.$content.'</h1>';
		break;
		case 'h2' :
			return '<h2>'.$content.'</h2>';
		break;
		case 'h3' :
			return '<h3>'.$content.'</h3>';
		break;
		case 'h4' :
			return '<h4>'.$content.'</h4>';
		break;
		case 'h5' :
			return '<h5>'.$content.'</h5>';
		break;
		case 'h6' :
			return '<h6>'.$content.'</h6>';
		break;
	}
}

add_shortcode('blogoma_title', 'add_blogoma_title');

/* ==========================================================================
   Highlight
========================================================================== */

function add_blogoma_highlight($atts, $content = null) {
 
	$highlight = '<span class="highlight">' . $content .'</span>';
					
	return $highlight;
}

add_shortcode('blogoma_highlight', 'add_blogoma_highlight');

/* ==========================================================================
   Alert Message
========================================================================== */

function add_blogoma_alert($atts, $content = null) {
		extract(shortcode_atts(array("type" => ''), $atts));  
		
		$message = '<div class="alert '.$type.'">';
		$message .= do_shortcode($content);
		$message .= '</div>';
		
		return $message;
}

add_shortcode('blogoma_alert', 'add_blogoma_alert');

/* ==========================================================================
   Divider
========================================================================== */

function add_blogoma_divider($atts, $content = null) {
	extract(shortcode_atts(array("type" => ''), $atts));  
		 
   	return '<hr class="'.$type.'">';
}

add_shortcode('blogoma_divider', 'add_blogoma_divider');

/* ==========================================================================
   Blockquote
========================================================================== */

function add_blogoma_blockquote($atts, $content = null) {	 
	extract(shortcode_atts(array(), $atts));  
   	return '<div class="blockquote-holder"><blockquote>'. $content .'</blockquote></div>';
}

add_shortcode('blogoma_blockquote', 'add_blogoma_blockquote');


/* ==========================================================================
   First Paragraph
========================================================================== */

function add_blogoma_first_paragraph($atts, $content = null) {	 
	extract(shortcode_atts(array(), $atts));  
   	return '<p class="first-p">'. do_shortcode($content ) .'</p>';
}

add_shortcode('blogoma_first_paragraph', 'add_blogoma_first_paragraph');

/* ==========================================================================
   Spacer
========================================================================== */

function add_blogoma_space() {	 
   	return '<div class="clearfix"></div>';
}

add_shortcode('blogoma_space', 'add_blogoma_space');



?>