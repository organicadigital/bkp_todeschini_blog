<?php 
	$shortcodes = array(
		array(
			'id' => 'Column',
			'description' => __('add a column on your page or post content' , 'blogoma'),
			'fields' => array(
				array(
					'id' => 'Column type',
					'type' => 'select' ,
					'options' => '1/2,1/3,2/3,1/4,3/4',
					'description' => __('Column proposition' , 'blogoma')
				),
				array(
					'id' => 'Last item',
					'type' => 'select' ,
					'options' => 'no, yes',
					'description' => __('it clears float for last column' , 'blogoma')
				),
			 )	
		),
		array(
			'id' => 'First Paragraph',
			'description' => __('Add a first paragraph content' , 'blogoma'),
			'fields'=> false
		),
		array(
			'id' => 'Title',
			'description' => __('Add a title' , 'blogoma'),
			'fields'=> array(
				array(
					'id' => 'type',
					'type' => 'select' ,
					'options' => 'h1,h2,h3,h4,h5,h6',
					'description' => __('H tag size' , 'blogoma')
				),
			 )	
		),
		array(
			'id' => 'Highlight',
			'description' => __('Add a highlighted content' , 'blogoma'),
			'fields'=> false
		),

		array(
			'id' => 'Dropcap',
			'description' => __('Add a dropcap' , 'blogoma'),
			'fields'=> array(
				array(
					'id' => 'style',
					'type' => 'select' ,
					'options' => 'circle,square',
					'description' => __('dropcap style' , 'blogoma')
				),
				array(
					'id' => 'color',
					'type' => 'text' ,
					'options' => '',
					'description' => __('Hex code ie; #fff' , 'blogoma')
				),
			 )	
		),	
		array(
			'id' => 'Divider',
			'description' => __('Add a divider' , 'blogoma'),
			'fields'=> array(
				array(
					'id' => 'type',
					'type' => 'select' ,
					'options' => 'thin,fat,dashed,dotted,stylish,shadow',
					'description' => __('Divider type' , 'blogoma')
				)
			 )	
		),
		array(
			'id' => 'Blockquote',
			'description' => __('Add a blockquote' , 'blogoma'),
			'fields'=> false
		),
		array(
			'id' => 'Space',
			'description' => __('Add a space' , 'blogoma'),
			'fields'=> false
		),
		array(
			'id' => 'Alert',
			'description' => __('Add a alert box' , 'blogoma'),
			'fields'=> array(
				array(
					'id' => 'type',
					'type' => 'select' ,
					'options' => 'alert-warning,alert-danger,alert-info,alert-success',
					'description' => __('Alert type' , 'blogoma')
				)
			 )	
		),	
	); 
?>