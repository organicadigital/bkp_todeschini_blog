
<div class="related-post-holder">
	<?php 
		$orig_post = $post;
		global $post;
		$categories = get_the_category($post->ID);

		if ($categories) {
			$category_ids = array();
			foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			$args=array(
			'category__in' => $category_ids,
			'post__not_in' => array($post->ID),
			'posts_per_page'=> 3, // Number of related posts that will be displayed.
			'ignore_sticky_posts'=>1,
			'orderby'=>'rand' // Randomize the posts
			);
			$my_query = new wp_query( $args );
			if( $my_query->have_posts() ) {
				echo '<div id="related_posts" class="clear"><h3>Artigos Relacionados</h3><ul>';
			while( $my_query->have_posts() ) {
				$my_query->the_post(); 
	?>
		<li>
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'related-post' ); ?>
		        <img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo get_the_title(); ?>">
		 	<?php else: ?>
		 		<?php 
		 			// if post type has not a featured image you'll show no image placeholder
			 		$format = get_post_format();
					if ( false === $format ) {
						$format = 'standard';
					}
					if($format == "standard"){
						$post_format_img =  get_template_directory_uri()."/images/post-format-standart-bg.png";
					}
					if($format == "audio"){
						$post_format_img =  get_template_directory_uri()."/images/post-format-auido-bg.png";
					}	
					if($format == "video"){
						$post_format_img =  get_template_directory_uri()."/images/post-format-video-bg.png";
					}		
					if($format == "quote"){
						$post_format_img =  get_template_directory_uri()."/images/post-format-quote-bg.png";
					}
					if($format == "link"){
						$post_format_img =  get_template_directory_uri()."/images/post-format-link-bg.png";
					}
					if($format == "status"){
						$post_format_img =  get_template_directory_uri()."/images/post-format-status-bg.png";
					}
				?>
				<img src="<?php echo esc_url($post_format_img); ?>" alt="<?php echo get_the_title(); ?>">
			<?php endif; ?>
		 	<div class="related-content">
		 		<span>
		 			<a href="<? the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		 		</span>
		 	</div>
		</li>
	<? }
		echo '</ul></div>';
	} }
		$post = $orig_post;
		wp_reset_query(); 
	?>
</div>	
<div class="post-paper-bg"></div>	
