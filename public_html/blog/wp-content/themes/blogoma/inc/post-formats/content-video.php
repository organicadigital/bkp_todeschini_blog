<?php
/**
 * @package blogoma
 */

$options 				= get_option('blogoma_admin'); 
$read_more				= $options['readmore_show'];
$show_tags				= $options['tags_show'];
$show_author			= $options['author_show'];
$related_content		= $options['related_content'];
$show_share				= $options['show_share'];
$show_like				= $options['show_like'];

$layout					= $options['blog_layout'];
$blog_type				= "normal";

$column					=  "";

?>
<div id="post-holder" class="<?php if(!	is_single()) { echo esc_attr($column); } ?>">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php if(!is_archive()) : ?>
	
	<div class="video-holder">
		<?php 
			$blogoma_video = get_post_meta($post->ID, 'blogoma_post_video_mp4', true);
			$blogoma_video_image = get_post_meta($post->ID, 'blogoma_post_video_img', true);
			$custom_embed = get_post_meta($post->ID, 'blogoma_post_cs_embed', true);
			
			if(!empty($custom_embed))
			{
				echo '<div class="video-holder">' . stripslashes(htmlspecialchars_decode($custom_embed)) . '</div>';
			}
			else
			{
				if(!empty($blogoma_video))
				{
					echo '<div class="vid-holder"><video src="'.$blogoma_video.'"id="videoPlayer-'.$post->ID.'" poster="'.$blogoma_video_image.'" controls="controls" preload="none"></video></div>';
				}
			}
		?>
	</div>

	<?php endif; ?>


	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php blogoma_posted_on(); ?>

			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
				<?php
					/* translators: used between list items, there is a space after the comma */
					$categories_list = get_the_category_list( __( ', ', 'blogoma' ) );
					if ( $categories_list && blogoma_categorized_blog() ) :
				?>
				<span class="cat-links">
					<?php printf( __( 'Posted in %1$s', 'blogoma' ), $categories_list ); ?>
				</span>
				<?php endif; // End if categories ?>

				<?php if ( !is_single() ) : // don't show in single ?>
					<?php if ($show_tags) : ?>
						<?php
							/* translators: used between list items, there is a space after the comma */
							$tags_list = get_the_tag_list( '', __( ', ', 'blogoma' ) );
							if ( $tags_list ) :
						?>
						<span class="tags-links">
							<?php printf( __( 'Tagged %1$s', 'blogoma' ), $tags_list ); ?>
						</span>
						<?php endif; // End if $tags_list ?>
					<?php endif ?>
				<?php endif; //end single control ?>
			<?php endif; // End if 'post' == get_post_type() ?>

		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content"><?php the_content(); ?></div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-footer">
		<?php if ( is_single() ) : // show tags in single ?>
			<?php if ($show_tags) : ?>
				<?php
					/* translators: used between list items, there is a space after the comma */
					$tags_list = get_the_tag_list( '', __( ' ', 'blogoma' ) );
					if ( $tags_list ) :
				?>
				<span class="tags-links">
					<?php printf( __( '%1$s', 'blogoma' ), $tags_list ); ?>
				</span>
				<?php endif; // End if $tags_list ?>
			<?php endif; // End if $tags_list ?>
		<?php endif; //end single control ?>
		
		<?php if ( !is_single() ) : // don't show in single ?>
			<?php if($read_more) : ?>
				<div class="read-more">
					<a href="<?php echo get_permalink(); ?>"> 
						<?php echo __( 'Continue reading', 'blogoma' ) ?>
					</a>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<div class="social-widgets-holder">
			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) && !is_single() ) : ?>
				<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'blogoma' ), __( '1 Comment', 'blogoma' ), __( '% Comments', 'blogoma' ) ); ?></span>
			<?php endif; ?>
			<?php if($show_like) : ?>
				<div class="post-like">
				    <?php echo blogoma_getPostLikeLink(get_the_ID());?>
				</div>
			<?php endif; ?>
			<?php if($show_share) : ?>
				<div class="post-share">
					<?php if(function_exists('blogoma_share')) blogoma_share(); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="clearfix"></div>

		<?php if(is_single()):  // show author if is single... ?>
			<?php if($show_author) : ?>
				<div class="author-holder">
					<div class="photo-holder">
					<?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), 80 ); }?>
					</div>
					<div class="info">
						<h3><?php the_author_meta('nickname') ?></h3>
						<p><?php  $description = the_author_meta('description');  ?></p>
					</div>
					<div class="clearfix"></div>
				</div>	
			<?php endif; ?>
		<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<div class="post-paper-bg"></div>
<?php if(is_single()) : ?>
	<?php if($related_content) : ?>
		<?php
			get_template_part( '/inc/post-formats/template-parts/related', 'post' );
		?>
	<?php endif; ?>
<?php endif; ?>


</div>