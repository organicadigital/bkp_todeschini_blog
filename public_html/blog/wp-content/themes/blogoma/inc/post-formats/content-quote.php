<?php
/**
 * @package blogoma
 */

$options 				= get_option('blogoma_admin'); 
$read_more				= $options['readmore_show'];
$show_tags				= $options['tags_show'];
$show_author			= $options['author_show'];
$related_content		= $options['related_content'];
$show_share				= $options['show_share'];
$show_like				= $options['show_like'];

$layout					= $options['blog_layout'];
$blog_type				= "normal";

$column					=  "";

?>
<div id="post-holder" class="<?php if(!	is_single()) { echo esc_attr($column); } ?>">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php 
			$blockquote 	= get_post_meta( get_the_ID(), 'blogoma_post_quote', true );
			$author			= get_post_meta( get_the_ID(), 'blogoma_post_quote_author', true );
			$link			= get_post_meta( get_the_ID(), 'blogoma_post_quote_link', true );
			$bg				= get_post_meta( get_the_ID(), 'blogoma_quote_bg_image', true );
	?>


	<div class="quote-holder"  <?php if($bg){ echo 'style="background:url('.$bg.') 0 0 transparent no-repeat;"'; }?>>
		<div class="bg"></div>
		<div class="quote-content">
			<blockquote>
				<p>
					<?php echo $blockquote; ?>
				</p>
				<cite>
					<?php
						if( !empty($link))
						{
							echo '<a href='. $link .' target="_blank">'. $author .'</a>';
						}
						else
						{
							echo $author;
						}
					?>
				</cite>
			</blockquote>
		</div>
	</div>

	
	<footer class="entry-footer">
		<?php if ( is_single() ) : // show tags in single ?>
			<?php if ($show_tags) : ?>
				<?php
					/* translators: used between list items, there is a space after the comma */
					$tags_list = get_the_tag_list( '', __( ' ', 'blogoma' ) );
					if ( $tags_list ) :
				?>
				<span class="tags-links">
					<?php printf( __( '%1$s', 'blogoma' ), $tags_list ); ?>
				</span>
				<?php endif; // End if $tags_list ?>
			<?php endif; // End if $tags_list ?>
		<?php endif; //end single control ?>
		
		<?php if ( !is_single() ) : // don't show in single ?>
			<?php if($read_more) : ?>
				<div class="read-more">
					<a href="<?php echo get_permalink(); ?>"> 
						<?php echo __( 'Continue reading', 'blogoma' ) ?>
					</a>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<div class="social-widgets-holder">
			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) && !is_single() ) : ?>
				<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'blogoma' ), __( '1 Comment', 'blogoma' ), __( '% Comments', 'blogoma' ) ); ?></span>
			<?php endif; ?>
			<?php if($show_like) : ?>
				<div class="post-like">
				    <?php echo blogoma_getPostLikeLink(get_the_ID());?>
				</div>
			<?php endif; ?>
			<?php if($show_share) : ?>
				<div class="post-share">
					<?php if(function_exists('blogoma_share')) blogoma_share(); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="clearfix"></div>

		<?php if(is_single()):  // show author if is single... ?>
			<?php if($show_author) : ?>
				<div class="author-holder">
					<div class="photo-holder">
					<?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), 80 ); }?>
					</div>
					<div class="info">
						<h3><?php the_author_meta('nickname') ?></h3>
						<p><?php  $description = the_author_meta('description');  ?></p>
					</div>
					<div class="clearfix"></div>
				</div>	
			<?php endif; ?>
		<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<div class="post-space"></div>
<?php if(is_single()) : ?>
	<?php if($related_content) : ?>
		<?php
			get_template_part( '/inc/post-formats/template-parts/related', 'post' );
		?>
	<?php endif; ?>
<?php endif; ?>


</div>