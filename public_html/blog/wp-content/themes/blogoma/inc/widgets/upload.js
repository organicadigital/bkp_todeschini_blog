jQuery(document).ready(function()
{
	// media manager holder
  var themetica_widget_upload;
    // when click on the upload button
    jQuery('.upload-image-btn').live('click' , function(e){

        // json field
        var field = jQuery(this).prev('.upload-image-field');
        field.val("");
       
        e.preventDefault();

        // open the frame
        if(themetica_widget_upload){
            themetica_widget_upload.open();
            return ;
        }

        // create the media frame
        themetica_widget_upload = wp.media.frames.themetica_widget_upload = wp.media({
            className : 'media-frame themetica-media-manager' ,
            multiple: false,
            title : 'Select Images' ,
            button : {
              text : 'Select'
            }
        });


        themetica_widget_upload.on('select', function(){
            var selection = themetica_widget_upload.state().get('selection');
              selection.map( function( attachment ) {
                  attachment = attachment.toJSON();
                  // insert images to the  feild
                  field.val(attachment.url);
            });
        });

        // Now that everything has been set, let's open up the frame.
        themetica_widget_upload.open();
    });
      // end upload script
});