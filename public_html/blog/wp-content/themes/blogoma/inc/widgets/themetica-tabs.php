<?php

/**
* Plugin Name: Themetica Tabs
* Description: A widget that displays your latest, popular and random posts...
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_recent_tabs');

function themetica_recent_tabs()
{
	register_widget('Recent_tabs_widget');
}

class Recent_tabs_widget extends WP_Widget {
	
	function Recent_tabs_widget()
	{
		$widget_ops = array('classname' => 'recent_tabs', 'description' => 'Custom recent tabs for sidebar.');

		$control_ops = array('id_base' => 'recent-posts-widget');

		$this->WP_Widget('recent-posts-widget', 'Blogoma // Tabs :', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', "");
		
		$number = $instance['number'];
		$latest = $instance['latest'];
		$popular = $instance['popular'];
		$random = $instance['random'];
		$tags = $instance['tags'];
		$tags_count = $instance['tags_count'];

		$title_length = 70;
		
		echo $before_widget;

		if($title) {
			echo $before_title . $title . $after_title;
		}
		
		?>
		<ul class="tabs-nav">
			<?php if($latest == "latest") : ?>
				<li><a href="javascript:;" data-tab-href="recent"><?php echo __( 'Latest', 'blogoma' ); ?></a></li>
			<?php endif ?>
			<?php if($popular == "popular") : ?>
				<li><a href="javascript:;" data-tab-href="popular"><?php echo __( 'Popular', 'blogoma' ); ?></a></li>
			<?php endif ?>
			<?php if($random == "random") : ?>
				<li><a href="javascript:;" data-tab-href="random"><?php echo __( 'Random', 'blogoma' ); ?></a></li>
			<?php endif ?>
			<?php if($tags == "tags") : ?>
				<li><a href="javascript:;" data-tab-href="tags"><?php echo __( 'Tags', 'blogoma' ); ?></a></li>
			<?php endif ?>
		</ul>

		<?php if($latest == "latest") : ?>
			<ul  class="recent-tab tab show_tab"> 
			
			<?php 
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => $number,
					'order' => 'DESC',
					'tax_query' => array(
					    array(
					      'taxonomy' => 'post_format',
					      'field' => 'slug',
					      'terms' => 'post-format-quote',
					      'operator' => 'NOT IN'
					    ),
					    array(
					      'taxonomy' => 'post_format',
					      'field' => 'slug',
					      'terms' => 'post-format-link',
					      'operator' => 'NOT IN'
					    )
					  )
				);

				$wp_query = new WP_Query($args);
				while ($wp_query->have_posts()) : $wp_query->the_post(); 
			?>


			<?php

				if (strlen(the_title('','',FALSE)) > $title_length) {
					$title_short = substr(the_title('','',FALSE), 0, $title_length);
					preg_match('/^(.*)\s/s', $title_short, $matches);
				if ($matches[1]) $title_short = $matches[1];
					$title_short = $title_short.'...';
				}
				else
				{
					$title_short = the_title('','',FALSE);
				}
			?>
					<li>
						<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'widget-thumb' );
							$url = $thumb['0']; 

							if(!empty($url)) :
						?>
							<div class="pic">
								<img src="<?php echo esc_url($url); ?>" alt="<?php echo get_the_title(); ?>" />
							</div>
						<?php 
							else :
								$format = get_post_format();
									if ( false === $format ) {
										$format = 'standard';
									}

									if($format == "standard"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-standart-bg.png";
									}
									if($format == "audio"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-audio-bg.png";
									}	
									if($format == "video"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-video-bg.png";
									}		
									if($format == "quote"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-quote-bg.png";
									}
									if($format == "link"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-link-bg.png";
									}
									if($format == "status"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-status-bg.png";
									}
						?>
							<div class="pic">
								<img src="<?php echo esc_url($post_format_img); ?>" alt="<?php echo get_the_title(); ?>">
							</div>
					<?php endif; ?>
						<div class="txt-holder">
							<a title="<?php echo the_title() ?>" href="<?php the_permalink() ?>">
								<?php echo $title_short ?>
							</a>
							<span>
								<?php the_time('F j, Y'); ?>
								<strong><?php _e("AND", "blogoma"); ?></strong>
								<?php 
									$vote_count = get_post_meta(get_the_ID(), "votes_count", true);
									if($vote_count == "" || empty($vote_count)){
										$vote_count = "0";
									}
									echo $vote_count ." ". __("likes","blogoma");
								?>
							</span>
						</div>
						<div class="clearfix"></div>
					</li>		
			<?php endwhile; ?>
			</ul>
		<?php endif ?>

		<?php if($popular == "popular") : ?>
			<ul class="popular-tab tab">
				<?php 

				$pop_args = array(
					'posts_per_page' => $number, 
					'meta_key' => 'wpb_post_views_count', 
					'orderby' => 'meta_value_num',
					'order' => 'DESC',
					'tax_query' => array(
					    array(
					      'taxonomy' => 'post_format',
					      'field' => 'slug',
					      'terms' => 'post-format-quote',
					      'operator' => 'NOT IN'
					    ),
					    array(
					      'taxonomy' => 'post_format',
					      'field' => 'slug',
					      'terms' => 'post-format-link',
					      'operator' => 'NOT IN'
					    )
					  )
				);

				$popular_post = new WP_Query( $pop_args );

					while ( $popular_post->have_posts() ) : $popular_post->the_post();
				?>

					<?php

						if (strlen(the_title('','',FALSE)) > $title_length) {
							$title_short = substr(the_title('','',FALSE), 0, $title_length);
							preg_match('/^(.*)\s/s', $title_short, $matches);
						if ($matches[1]) $title_short = $matches[1];
							$title_short = $title_short.'...';
						}
						else
						{
							$title_short = the_title('','',FALSE);
						}
					?>

					<li>
						<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'widget-thumb' );
							$url = $thumb['0']; 
							if(!empty($url)):
						?>
							<div class="pic">
								<img src="<?php echo esc_url($url); ?>" alt="<?php echo get_the_title(); ?>" />
							</div>
						<?php 
							else :
								$format = get_post_format();
									if ( false === $format ) {
										$format = 'standard';
									}
									if($format == "standard"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-standart-bg.png";
									}
									if($format == "audio"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-audio-bg.png";
									}	
									if($format == "video"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-video-bg.png";
									}		
									if($format == "quote"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-quote-bg.png";
									}
									if($format == "link"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-link-bg.png";
									}
									if($format == "status"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-status-bg.png";
									}
						?>
							<div class="pic">
								<img src="<?php echo esc_url($post_format_img); ?>" alt="<?php echo get_the_title(); ?>">
							</div>
						<?php 
							endif;
						?>
						<div class="txt-holder">
							<a title="<?php echo the_title() ?>" href="<?php the_permalink() ?>">
								<?php echo $title_short ?>
							</a>
							<span>
								<?php the_time('F j, Y'); ?>
								<strong><?php _e("AND", "blogoma"); ?></strong>
								<?php 
									$vote_count = get_post_meta(get_the_ID(), "votes_count", true);
									if($vote_count == ""){
										$vote_count = "0";
									}
									echo $vote_count ." ". __("likes","blogoma");
								?>
							</span>
						</div>
						<div class="clearfix"></div>
					</li>	

				<?php
					endwhile;
				?>
			</ul>
		<?php endif ?>

		<?php if($random == "random") : ?>

			<ul class="random-tab tab">
				<?php 

				$random_args = array(
					'posts_per_page' => $number, 
					'orderby' => 'rand'   
				);

				$random_post = new WP_Query( $random_args );

					while ( $random_post->have_posts() ) : $random_post->the_post();
				?>

					<?php

						if (strlen(the_title('','',FALSE)) > $title_length) {
							$title_short = substr(the_title('','',FALSE), 0, $title_length);
							preg_match('/^(.*)\s/s', $title_short, $matches);
						if ($matches[1]) $title_short = $matches[1];
							$title_short = $title_short.'...';
						}
						else
						{
							$title_short = the_title('','',FALSE);
						}
					?>
			
					<li>
						<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'widget-thumb' );
							$url = $thumb['0']; 
							if(!empty($url)):
						?>

							<div class="pic">
								<img src="<?php echo esc_url($url); ?>" alt="<?php echo get_the_title(); ?>">
							</div>
						<?php 
							else :
								$format = get_post_format();
									if ( false === $format ) {
										$format = 'standard';
									}
									if($format == "standard"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-standart-bg.png";
									}
									if($format == "audio"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-audio-bg.png";
									}	
									if($format == "video"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-video-bg.png";
									}		
									if($format == "quote"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-quote-bg.png";
									}
									if($format == "link"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-link-bg.png";
									}
									if($format == "status"){
										$post_format_img =  get_template_directory_uri()."/images/tabs-status-bg.png";
									}
						?>
							<div class="pic">
								<img src="<?php echo esc_url($post_format_img); ?>" alt="<?php echo get_the_title(); ?>">
							</div>
						<?php 
							endif;
						?>
						<div class="txt-holder">
							<a title="<?php echo the_title() ?>" href="<?php the_permalink() ?>">
								<?php echo $title_short ?>
							</a>
							<span>
								<?php the_time('F j, Y'); ?>
								<strong><?php _e("AND", "blogoma"); ?></strong>
								<?php 
									$vote_count = get_post_meta(get_the_ID(), "votes_count", true);
									if($vote_count == ""){
										$vote_count = "0";
									}
									echo $vote_count ." ". __("likes","blogoma");
								?>
							</span>
						</div>
						<div class="clearfix"></div>
					</li>	

				<?php
					endwhile;
				?>
			</ul>
		<?php endif ?>

		<?php if($tags == "tags") : ?>
			<ul class="tags-tab tab">
				<?php 
					$tag_args = array(
						'smallest'=>10.5,
						'largest'=>16,
						'number'=>$tags_count,
						'orderby'=>'count'  
					);
					
					wp_tag_cloud($tag_args); 
				?>
			</ul>
		<?php endif ?>

		<?php echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['number'] = $new_instance['number'];
		$instance['latest'] = $new_instance['latest'];
		$instance['popular'] = $new_instance['popular'];
		$instance['random'] = $new_instance['random'];
		$instance['tags'] = $new_instance['tags'];
		$instance['tags_count'] = $new_instance['tags_count'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('latest'=>'latest', 'popular'=>'popular', 'random'=>'random', 'tags'=>'tags', 'number' => 5, 'tags_count'=>20 );
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label for="<?php echo $this->get_field_id('number'); ?>">Number of items:</label>
			<input style="width:40px;" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo esc_attr($instance['number']); ?>" />
		</p>
		<h4>
			Showing Tabs
		</h4>
		<p>
			<label for="<?php echo $this->get_field_id('latest'); ?>">Latest</label>
			<input 	type="checkbox" id="<?php echo $this->get_field_id('latest'); ?>" name="<?php echo $this->get_field_name('latest'); ?>" <?php if ($instance['latest'] == "latest" ) echo 'checked'; ?>	value="latest" >
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('popular'); ?>">Popular</label>
			<input 	type="checkbox" id="<?php echo $this->get_field_id('popular'); ?>" name="<?php echo $this->get_field_name('popular'); ?>" <?php if ($instance['popular'] == "popular" ) echo 'checked'; ?>	value="popular" >
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('random'); ?>">Random</label>
			<input 	type="checkbox" id="<?php echo $this->get_field_id('random'); ?>" name="<?php echo $this->get_field_name('random'); ?>" <?php if ($instance['random'] == "random" ) echo 'checked'; ?>	value="random" >
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('tags'); ?>">Tags</label>
			<input 	type="checkbox" id="<?php echo $this->get_field_id('tags'); ?>" name="<?php echo $this->get_field_name('tags'); ?>" <?php if ($instance['tags'] == "tags" ) echo 'checked'; ?>	value="tags" >
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('tags'); ?>">Number of tags</label>
			<input style="width:40px;" id="<?php echo $this->get_field_id('tags_count'); ?>" name="<?php echo $this->get_field_name('tags_count'); ?>" value="<?php echo esc_attr($instance['tags_count']); ?>" />
		</p>
	<?php
	}
}
?>