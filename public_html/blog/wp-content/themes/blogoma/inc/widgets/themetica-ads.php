<?php

/**
* Plugin Name: Themetica Ads
* Description: A widget that displays your sidebar banner
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_ads');

function themetica_ads()
{
	register_widget('Ads_widget');
}

class Ads_widget extends WP_Widget {
	
	function Ads_widget()
	{
		$widget_ops = array('classname' => 'ads', 'description' => 'blogoma Ads widget');

		$control_ops = array('id_base' => 'ads-widget');

		$this->WP_Widget('ads-widget', 'Blogoma // Ads :', $widget_ops, $control_ops);

		// add image js 
        add_action('admin_enqueue_scripts' , array(&$this , 'admin_js') , 99 , 1);
	}

	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$href = $instance['href'];
		$ad_img = $instance['ad_img'];

		echo $before_widget;

		if($title != "") {
			echo $before_title . $title . $after_title;
		}
		
		?>
		<a href="<?php echo esc_url($href); ?>" target="_blank">
			<img src="<?php echo esc_url($ad_img); ?>" alt="blogoma - Ad">
		</a>

		<?php echo $after_widget;
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];
		$instance['href'] = $new_instance['href'];
		$instance['ad_img'] = $new_instance['ad_img'];
		
		return $instance;
	}

	function admin_js($hook)
    {
      if($hook == 'widgets.php')
      { 
          wp_enqueue_media();
          wp_enqueue_script('wp_enqueue_script' , get_template_directory_uri() . '/inc/widgets/upload.js');
      }
    }

	function form($instance)
	{
		$defaults = array('title'=>'', 'href'=>'http://', 'ad_img'=>'');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>

		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('href')); ?>">Banner link (i.e: http://www.themetica.com ):</label>
			<input style="width:100%;"  id="<?php echo esc_attr($this->get_field_id('href')); ?>" name="<?php echo esc_attr($this->get_field_name('href')); ?>" value="<?php echo esc_attr($instance['href']); ?>" />
		</p>

		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('ad_img')); ?>">Banner Image URL:</label>
			<input class="upload-image-field"  style="width:100%;" id="<?php echo esc_attr($this->get_field_id('ad_img')); ?>" name="<?php echo esc_attr($this->get_field_name('ad_img')); ?>" value="<?php echo esc_attr($instance['ad_img']); ?>" />
			<a href="javascript:;" class="upload-image-btn">Add Image</a>
		</p>
		
	<?php
	}
}