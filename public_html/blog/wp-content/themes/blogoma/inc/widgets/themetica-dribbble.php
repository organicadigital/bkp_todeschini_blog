<?php

/**
* Plugin Name: Themetica Dribbble
* Description: A widget that displays your dribbble shots...
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_dribbble');

function themetica_dribbble()
{
	register_widget('Dribbble_widget');
}

class Dribbble_widget extends WP_Widget {
	
	function Dribbble_widget()
	{
		$widget_ops = array('classname' => 'dribbble-shots', 'description' => 'Latest Dribbble shots for sidebar.');

		$control_ops = array('id_base' => 'dribbble-shots-widget');

		$this->WP_Widget('dribbble-shots-widget', 'Blogoma // Dribbble :', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$user = $instance['user'];
		$number = $instance['number'];

		echo $before_widget;

		if($title) {
			echo $before_title . $title . $after_title;
		}
		
		?>
		
		<!-- Content -->
		<?php
			wp_enqueue_script( 'jribbble', get_template_directory_uri() . '/js/jquery.jribbble-1.0.1.ugly.js', array(), '1.0.1', true );
		?>
		
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			$.jribbble.getShotsByPlayerId('<?php echo $user; ?>', function (playerShots) {
			    var html = [];

			    $.each(playerShots.shots, function (i, shot) {
			        html.push('<li><a href="' + shot.url + '" target="_blank">');
			        html.push('<img src="' + shot.image_url + '" ');
			        html.push('alt="' + shot.title + '"></a></li>');
			    });

			    $('#shotsByPlayerId').html(html.join(''));

				$('.dribbble-slider').flexslider({
		            animation: 'fade',
		            controlNav: false,
    				slideshow: false,
		            prevText: "Prev",
		            nextText: "Next"
	            });

			}, {page: 1, per_page: <?php echo $number; ?>});
		});

		</script>

		<div class="dribbble-slider flexslider"> 
		  	  <ul id="shotsByPlayerId"  class="slides">
		  	  </ul>
		</div>
			
		<!-- End Content-->

		<?php echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];

		$instance['number'] = $new_instance['number'];
		$instance['user'] = $new_instance['user'];
		
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title'=>'', 'user'=>'', 'number'=>5 );
		$instance = wp_parse_args((array) $instance, $defaults); ?>	

		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<h4>
			Dribbble Settings
		</h4>
		<p>
			<label style="display:block;" for="<?php echo esc_attr($this->get_field_id('user')); ?>">Username: </label>
			<input style="width:50%;" id="<?php echo esc_attr($this->get_field_id('user')); ?>" name="<?php echo esc_attr($this->get_field_name('user')); ?>" value="<?php echo esc_attr($instance['user']); ?>" />
		</p>
		<p>
			<label style="display:block;" for="<?php echo esc_attr($this->get_field_id('number')); ?>">Number of items:</label>
			<input style="width:40px;" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" value="<?php echo esc_attr($instance['number']); ?>" />
		</p>
		
	<?php
	}
}
?>