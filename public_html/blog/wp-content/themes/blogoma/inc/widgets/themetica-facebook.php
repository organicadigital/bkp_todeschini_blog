<?php

/**
* Plugin Name: Themetica Facebook
* Description: A widget that displays your facebook page likes, post and users
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_facebook');

function themetica_facebook()
{
	register_widget('Themetica_facebook_widget');
}

class Themetica_facebook_widget extends WP_Widget {
	
	function Themetica_facebook_widget()
	{
		$widget_ops = array('classname' => 'facebook-widgets', 'description' => 'Blogoma facebook widget');

		$control_ops = array('id_base' => 'facebook-widget');

		$this->WP_Widget('facebook-widget', 'Blogoma // Facebook :', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		
		$url 	= $instance['page_url'];
		$width 	= $instance['width'];
		$height = $instance['height'];
		$scheme = $instance['scheme'];
		$faces 	= $instance['faces'];
		$posts 	= $instance['posts'];
		$header = $instance['header'];
		$border = $instance['border'];

		echo $before_widget;

		if($title != "") {
			echo $before_title . $title . $after_title;
		}
		
		?>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=549582328459074&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<div class="fb-like-box" 
				data-href="<?php echo esc_attr($url); ?>" 
				data-height="<?php echo esc_attr($height); ?>" 
				data-colorscheme="<?php echo esc_attr($scheme); ?>" 
				data-show-faces="<?php if($faces){echo "true";}else{echo "false";}; ?>" 
				data-header="<?php if($header){echo "true";}else{echo "false";}; ?>" 
				data-stream="<?php if($posts){echo "true";}else{echo "false";}; ?>" 
				data-show-border="<?php if($border){echo "true";}else{echo "false";}; ?>">
		</div>

		<?php echo $after_widget;
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];
		$instance['page_url'] = $new_instance['page_url'];
		$instance['width'] = $new_instance['width'];
		$instance['height'] = $new_instance['height'];
		$instance['scheme'] = $new_instance['scheme'];
		$instance['faces'] = $new_instance['faces'];
		$instance['posts'] = $new_instance['posts'];
		$instance['header'] = $new_instance['header'];
		$instance['border'] = $new_instance['border'];
		
		return $instance;
	}


	function form($instance)
	{
		$defaults = array('title'=>'', 'page_url'=>'http://', 'width'=>'', 'height'=>'', 'scheme'=>'', 'faces'=>'', 'posts'=>'', 'header'=>'', 'border'=>'');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>

		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('page_url'); ?>">Facebook Page URL:</label>
			<input style="width:100%;" id="<?php echo $this->get_field_id('page_url'); ?>" name="<?php echo $this->get_field_name('page_url'); ?>" value="<?php echo esc_attr($instance['page_url']); ?>" />		
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('height'); ?>">Height: </label>
			<span style="color:#666;font-size:12px;">The pixel height of the plugin</span>
			<input style="width:80%; display:block;" id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" value="<?php echo esc_attr($instance['height']); ?>" />		
		</p>

		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('scheme'); ?>">Color Scheme: </label>
			<select id="<?php echo $this->get_field_id( 'scheme' ); ?>" name="<?php echo $this->get_field_name( 'scheme' ); ?>">
			     <option value="light" <?php if ($instance['scheme'] == "light" ) echo 'selected="selected"'; ?> >Light</option>
			     <option value="dark" <?php if ($instance['scheme'] == "dark" ) echo 'selected="selected"'; ?> >Dark</option>
			</select>
		</p>
		<p style="width:50%; float:left">
			<input 	type="checkbox" id="<?php echo $this->get_field_id('faces'); ?>" name="<?php echo $this->get_field_name('faces'); ?>" <?php if ($instance['faces'] == "true" ) echo 'checked'; ?>	value="true" >
			<label for="<?php echo $this->get_field_id('faces'); ?>">Show Friends' Faces</label>
		</p>
		<p style="width:50%; float:left">
			<input 	type="checkbox" id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" <?php if ($instance['posts'] == "true" ) echo 'checked'; ?>	value="true" >
			<label for="<?php echo $this->get_field_id('posts'); ?>">Show Posts</label>
		</p>
		<p style="width:50%; float:left">
			<input 	type="checkbox" id="<?php echo $this->get_field_id('header'); ?>" name="<?php echo $this->get_field_name('header'); ?>" <?php if ($instance['header'] == "true" ) echo 'checked'; ?>	value="true" >
			<label for="<?php echo $this->get_field_id('header'); ?>">Show Header</label>
		</p>
		<p style="width:50%; float:left">
			<input 	type="checkbox" id="<?php echo $this->get_field_id('border'); ?>" name="<?php echo $this->get_field_name('border'); ?>" <?php if ($instance['border'] == "true" ) echo 'checked'; ?>	value="true" >
			<label for="<?php echo $this->get_field_id('border'); ?>">Show Border</label>
		</p>
		
		
	<?php
	}
}