<?php

/**
* Plugin Name: Themetica Social
* Description: A widget that displays your social accounts on sidebar
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_social');

function themetica_social()
{
	register_widget('Social_widget');
}

class Social_widget extends WP_Widget {
	
	function Social_widget()
	{
		$widget_ops = array('classname' => 'social-widget', 'description' => 'Blogoma social widget');

		$control_ops = array('id_base' => 'social-widget');

		$this->WP_Widget('social-widget', 'Blogoma // Social :', $widget_ops, $control_ops);

	}

	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$target 	= $instance['target'];

		$facebook 	= $instance['facebook'];
		$twitter 	= $instance['twitter'];
		$behance 	= $instance['behance'];
		$dribbble 	= $instance['dribbble'];
		$instagram 	= $instance['instagram'];
		$pinterest 	= $instance['pinterest'];
		$google 	= $instance['google'];
		$youtube 	= $instance['youtube'];
		$flickr 	= $instance['flickr'];
		$soundcloud = $instance['soundcloud'];
		$linkedin 	= $instance['linkedin'];
		$rss 		= $instance['rss'];

		echo $before_widget;

		if($title != "") {
			echo $before_title . $title . $after_title;
		}
		
		?>

		<ul>
			<?php if($facebook) : ?>
				<li>
					<a href="<?php echo esc_url($facebook); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe227;" alt="Facebook"></a>
				</li>
			<?php endif; ?>
			<?php if($twitter) : ?>
				<li>
					<a href="<?php echo esc_url($twitter); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe286;" alt="Twitter"></a>
				</li>
			<?php endif; ?>
			<?php if($behance) : ?>
				<li>
					<a href="<?php echo esc_url($behance); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe209;" alt="Behance"></a>
				</li>
			<?php endif; ?>
			<?php if($dribbble) : ?>
				<li>
					<a href="<?php echo esc_url($dribbble); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe221;" alt="Dribbble"></a>
				</li>
			<?php endif; ?>
			<?php if($instagram) : ?>
				<li>
					<a href="<?php echo esc_url($instagram); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe300;" alt="Instagram"></a>
				</li>
			<?php endif; ?>
			<?php if($pinterest) : ?>
				<li>
					<a href="<?php echo esc_url($pinterest); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe264;" alt="Pinterest"></a>
				</li>
			<?php endif; ?>
			<?php if($google) : ?>
				<li>
					<a href="<?php echo esc_url($google); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe239;" alt="Google+"></a>
				</li>
			<?php endif; ?>
			<?php if($youtube) : ?>
				<li>
					<a href="<?php echo esc_url($youtube); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe299;" alt="Youtube"></a>
				</li>
			<?php endif; ?>
			<?php if($flickr) : ?>
				<li>
					<a href="<?php echo esc_url($flickr); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe229;" alt="Flickr"></a>
				</li>
			<?php endif; ?>
			<?php if($soundcloud) : ?>
				<li>
					<a href="<?php echo esc_url($soundcloud); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe278;" alt="SoundCloud"></a>
				</li>
			<?php endif; ?>
			<?php if($linkedin) : ?>
				<li>
					<a href="<?php echo esc_url($linkedin); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe252;" alt="Linkedin"></a>
				</li>
			<?php endif; ?>
			<?php if($rss) : ?>
				<li>
					<a href="<?php echo esc_url($rss); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe271;" alt="RSS"></a>
				</li>
			<?php endif; ?>
		</ul>

		<?php echo $after_widget;
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] 		= $new_instance['title'];
		$instance['target'] 	= $new_instance['target'];

		$instance['facebook']	= $new_instance['facebook'];
		$instance['twitter']	= $new_instance['twitter'];
		$instance['behance']	= $new_instance['behance'];
		$instance['dribbble']	= $new_instance['dribbble'];
		$instance['instagram']	= $new_instance['instagram'];
		$instance['pinterest']	= $new_instance['pinterest'];
		$instance['google']		= $new_instance['google'];
		$instance['youtube']	= $new_instance['youtube'];
		$instance['flickr']		= $new_instance['flickr'];
		$instance['soundcloud']	= $new_instance['soundcloud'];
		$instance['linkedin']	= $new_instance['linkedin'];
		$instance['rss']		= $new_instance['rss'];
		
		return $instance;
	}

	function form($instance) { $defaults = array('title'=>'',
	'facebook'=>'', 'twitter'=>'', 'behance'=>'',
	'dribbble'=>'', 'instagram'=>'', 'pinterest'=>'',
	'google'=>'', 'youtube'=>'', 'flickr'=>'',
	'soundcloud'=>'', 'linkedin'=>'', 'target'=>'_blank', 'rss'=>'', 'target'=>'_blank');
	$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<h4>Social Accounts</h4>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('facebook'); ?>">Facebook: </label>
			<span style="color:#666;font-size:12px;">(ie; https://www.facebook.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" value="<?php echo esc_attr($instance['facebook']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('twitter'); ?>">Twitter:</label>
			<span style="color:#666;font-size:12px;">(ie; https://www.twitter.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" value="<?php echo esc_attr($instance['twitter']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('behance'); ?>">Behance:</label>
			<span style="color:#666;font-size:12px;">(ie; https://wwww.behance.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('behance'); ?>" name="<?php echo $this->get_field_name('behance'); ?>" value="<?php echo esc_attr($instance['behance']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('dribbble'); ?>">Dribble:</label>
			<span style="color:#666;font-size:12px;">(ie; https://wwww.dribbble.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('dribbble'); ?>" name="<?php echo $this->get_field_name('dribbble'); ?>" value="<?php echo esc_attr($instance['dribbble']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('instagram'); ?>">Instagram:</label>
			<span style="color:#666;font-size:12px;">(ie; https://www.instagram.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('instagram'); ?>" name="<?php echo $this->get_field_name('instagram'); ?>" value="<?php echo esc_attr($instance['instagram']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('pinterest'); ?>">Pinterest:</label>
			<span style="color:#666;font-size:12px;">(ie; https://www.pinterest.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('pinterest'); ?>" name="<?php echo $this->get_field_name('pinterest'); ?>" value="<?php echo esc_attr($instance['pinterest']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('google'); ?>">Google+:</label>
			<span style="color:#666;font-size:12px;">(ie; https://plus.google.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('google'); ?>" name="<?php echo $this->get_field_name('google'); ?>" value="<?php echo esc_attr($instance['google']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('youtube'); ?>">Youtube:</label>
			<span style="color:#666;font-size:12px;">(ie; https://wwww.youtube.com/user/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>" value="<?php echo esc_attr($instance['youtube']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('flickr'); ?>">Flickr:</label>
			<span style="color:#666;font-size:12px;">(ie; https://www.flickr.com/photos/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('flickr'); ?>" name="<?php echo $this->get_field_name('flickr'); ?>" value="<?php echo esc_attr($instance['flickr']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('soundcloud'); ?>">Soundcloud:</label>
			<span style="color:#666;font-size:12px;">(ie; https://wwww.soundcloud.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('soundcloud'); ?>" name="<?php echo $this->get_field_name('soundcloud'); ?>" value="<?php echo esc_attr($instance['soundcloud']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('linkedin'); ?>">Linkedin:</label>
			<span style="color:#666;font-size:12px;">(ie; https://www.linkedin.com/username )</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('linkedin'); ?>" name="<?php echo $this->get_field_name('linkedin'); ?>" value="<?php echo esc_attr($instance['linkedin']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('rss'); ?>">RSS:</label>
			<span style="color:#666;font-size:12px;">(ie; http://www.yoursite.com/feed/)</span>
			<input style="width:100%;" id="<?php echo $this->get_field_id('rss'); ?>" name="<?php echo $this->get_field_name('rss'); ?>" value="<?php echo esc_attr($instance['rss']); ?>" />
		</p>
		<p>
			<input 	type="checkbox" id="<?php echo $this->get_field_id('target'); ?>" name="<?php echo $this->get_field_name('target'); ?>" <?php if ($instance['target'] == "_blank" ) echo 'checked'; ?>	value="_blank" >
			<label for="<?php echo $this->get_field_id('target'); ?>">Open New Page</label>
		</p>
	<?php
	}
}