<?php

/**
* Plugin Name: Themetica Instagram
* Description: A widget that displays your instagram photos...
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_instagram');

function themetica_instagram()
{
	register_widget('Instagram_widget');
}

class Instagram_widget extends WP_Widget {
	
	function Instagram_widget()
	{
		$widget_ops = array('classname' => 'instagram-photos', 'description' => 'Latest Instagram photos for sidebar.');

		$control_ops = array('id_base' => 'instagram-photos-widget');

		$this->WP_Widget('instagram-photos-widget', 'Blogoma // Instagram :', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$user = $instance['user'];
		$token = $instance['token'];
		$number = $instance['number'];

		echo $before_widget;

		if($title) {
			echo $before_title . $title . $after_title;
		}
		
		if(($cache = wp_cache_get('themetica_instagram', 'blogoma')) === false){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);

			// get recent instagram posts
			curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/".$user."/media/recent/?count=".$number."&access_token=".$token);
			$posts_data = curl_exec($ch);

			// get follows, following and total images
			curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/".$user."?access_token=".$token);
			$user_data = curl_exec($ch);
			
			if(curl_errno($ch)){
				$curl_error = '<div class="pad15 alert alert-error" style="margin:0"><h4>Error</h4><p>'.curl_error($ch).'</p></div>'; 						    
			}

			curl_close($ch);

			$posts 	= json_decode($posts_data);
			$user 	= json_decode($user_data);

			if(!isset($posts->meta->error_message) or !isset($user->meta->error_message)){
				// save to cache for 6 hours
		        wp_cache_set( 'themetica_instagram', array('posts' => $posts_data, 'user' => $user_data), 'blogoma',0);
			}else{
				wp_cache_delete( 'themetica_instagram', 'blogoma' );
			}

	    }else{
			
			$posts 	= json_decode($cache['posts']);
			$user 	= json_decode($cache['user']);
	    }

		?>
		
		<!-- Content -->
		<div class="instagram-info">
			<span><strong><?php echo $user->data->counts->followed_by; ?></strong> <?php _e("Followers", "blogoma")?></span>
			<span><strong><?php echo $user->data->counts->follows; ?></strong> <?php _e("Following", "blogoma")?></span>
		  	<span><strong><?php echo $user->data->counts->media; ?></strong> <?php _e("Photos", "blogoma")?></span>
		</div>
		<div class="instagram-slider flexslider"> 
			<ul class="slides">
				<?php foreach ($posts->data as $post) { ?>
					<li>
						<img src="<?php echo $post->images->low_resolution->url ?>">
					</li>
				<?php } ?>
			</ul>
		</div>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('.instagram-slider').flexslider({
		            animation: 'fade',
		            controlNav: false,
					slideshow: true,
		            prevText: "Prev",
		            nextText: "Next"
	            });
			});
		</script>
			
		<!-- End Content-->

		<?php echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] 	= $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['user'] 	= $new_instance['user'];
		$instance['token'] 	= $new_instance['token'];
		
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title'=>'', 'user'=>'', 'number'=>5 , 'token'=>'');
		$instance = wp_parse_args((array) $instance, $defaults); ?>	

		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<h4>
			Instagram Settings
		</h4>
		<strong>Instructions</strong>
		  <p>You need to authenticate yourself to our instagram app to get an access token to retrieve your images and display them on your page.</p>
		  <ol>
		    <li>Attain your access token and user id <a href="https://api.instagram.com/oauth/authorize?client_id=9818def173f647a180feee84bcb0de49&redirect_uri=http%3A%2F%2Fthemetica.com%2Finstagram.php&scope=basic&response_type=code" target="_blank">by clicking here</a></li>
		    <li>Copy user id and the access token</li>
		    <li>Paste them in the input box below</li>
		  </ol>
		<p>
			<label style="display:block;" for="<?php echo $this->get_field_id('user'); ?>">User id: </label>
			<input style="width:50%;" id="<?php echo $this->get_field_id('user'); ?>" name="<?php echo $this->get_field_name('user'); ?>" value="<?php echo esc_attr($instance['user']); ?>" />
		</p>
		<p>
			<label style="display:block;" for="<?php echo $this->get_field_id('token'); ?>">Access Token: </label>
			<input style="width:50%;" id="<?php echo $this->get_field_id('token'); ?>" name="<?php echo $this->get_field_name('token'); ?>" value="<?php echo esc_attr($instance['token']); ?>" />
		</p>
		<p>
			<label style="display:block;" for="<?php echo $this->get_field_id('number'); ?>">Number of items:</label>
			<input style="width:40px;" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo esc_attr($instance['number']); ?>" />
		</p>
		
	<?php
	}
}
?>