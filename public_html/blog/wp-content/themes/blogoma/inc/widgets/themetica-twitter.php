<?php

/**
* Plugin Name: Themetica Twitter
* Description: A widget that displays your twitter timeline 
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_twitter');

function themetica_twitter()
{
	register_widget('Twitter_widget');
}

class Twitter_widget extends WP_Widget {
	
	function Twitter_widget()
	{
		$widget_ops = array('classname' => 'twitter-widgets', 'description' => 'Blogoma twitter widget');

		$control_ops = array('id_base' => 'twitter-widget');

		$this->WP_Widget('twitter-widget', 'Blogoma // Twitter :', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$code 	= $instance['code'];

		echo $before_widget;

		if($title != "") {
			echo $before_title . $title . $after_title;
		}
		
		?>

		<?php 
			if($code != "")
			{				
				echo $code;
			}
			else
			{
				echo '<p class="alert alert-warning">please, enter twitter timeline embed code!</p>';
			}
		?>

		<?php echo $after_widget;
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];
		$instance['code'] = $new_instance['code'];
		
		return $instance;
	}


	function form($instance)
	{
		$defaults = array('title'=>'', 'page_url'=>'http://', 'width'=>'', 'height'=>'', 'scheme'=>'', 'faces'=>'', 'posts'=>'', 'header'=>'', 'border'=>'');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>

		<p>
			<label style="display:block" for="<?php echo $this->get_field_id('code'); ?>">Twitter Embed Code:</label>
			<span style="color:#666;font-size:12px; display:block;">Please, create a widget for your site from <a href="https://twitter.com/settings/widgets/new" target="_blank">Twitter Widgets</a> and copy paste your embed code.</span>
			<textarea style="width:100%;height:150px" id="<?php echo $this->get_field_id('code'); ?>" name="<?php echo $this->get_field_name('code'); ?>"><?php echo $instance['code']; ?></textarea>
		</p>		
	<?php
	}
}