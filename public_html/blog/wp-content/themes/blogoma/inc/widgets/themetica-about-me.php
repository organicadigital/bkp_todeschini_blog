<?php

/**
* Plugin Name: Themetica About me
* Description: A widget that displays your about me box
* Version: 0.1
* Author: Themetica
* Author URI: http://themetica.com/
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('widgets_init', 'themetica_about_me');

function themetica_about_me()
{
	register_widget('About_me_widget');
}

class About_me_widget extends WP_Widget {
	
	function About_me_widget()
	{
		$widget_ops = array('classname' => 'about-me', 'description' => 'blogoma About me widget');

		$control_ops = array('id_base' => 'about-me-widget');

		$this->WP_Widget('about-me-widget', 'Blogoma // About me :', $widget_ops, $control_ops);

		// add image js 
        add_action('admin_enqueue_scripts' , array(&$this , 'admin_js') , 99 , 1);
	}

	function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$img = $instance['img'];
		$text = $instance['text'];

		echo $before_widget;

		if($title != "") {
			echo $before_title . $title . $after_title;
		}
		
		?>
		<?php if($img != "") : ?>
			<div class="photo-holder">
				<img src="<?php echo esc_url($img); ?>" alt="<?php _e("About me", "blogoma"); ?>">
			</div>
		<?php endif; ?>
		<p>
			<?php echo $text; ?>
		</p>

		<?php echo $after_widget;
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];
		$instance['img'] = $new_instance['img'];
		$instance['text'] = $new_instance['text'];
		
		return $instance;
	}

	function admin_js($hook)
    {
      if($hook == 'widgets.php')
      { 
          wp_enqueue_media();
          wp_enqueue_script('wp_enqueue_script' , get_template_directory_uri() . '/inc/widgets/upload.js');
      }
    }

	function form($instance)
	{
		$defaults = array('title'=>'', 'img'=>'', 'text'=>'About me...');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		
		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
			<input style="width:100%;" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('img')); ?>">Your Photo:</label>
			<input class="upload-image-field"  style="width:100%;" id="<?php echo esc_attr($this->get_field_id('img')); ?>" name="<?php echo esc_attr($this->get_field_name('img')); ?>" value="<?php echo esc_attr($instance['img']); ?>" />
			<a href="javascript:;" class="upload-image-btn">Add Image</a>
		</p>
		<p>
			<label style="display:block" for="<?php echo esc_attr($this->get_field_id('text')); ?>">Description Area: </label>
			<textarea style="width:100%;"  id="<?php echo esc_attr($this->get_field_id('text')); ?>" name="<?php echo esc_attr($this->get_field_name('text')); ?>"><?php echo $instance['text']; ?></textarea>
		</p>
		
	<?php
	}
}