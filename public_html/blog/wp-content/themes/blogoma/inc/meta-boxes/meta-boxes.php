<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category Themetica - blogoma Blog Theme
 * @package  Metaboxes
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 */

add_filter( 'cmb_meta_boxes', 'blogoma_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function blogoma_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'blogoma_';
	 
	/* ==========================================================================
	POST TYPE
	========================================================================== */
   
   /**
	*  META FOR FEATURED POST TYPE
	*/
	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_featured',
		'title'      => 'Featured Post Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'fields' => array(
			array(
				'name'    => 'Featured?',
				'desc'    => "is it a featured post? please, select it. (ps; don't forget to set a featured image for it from sidebar...)",
				'id'   => $prefix . 'featured_checkbox',
				'type' => 'checkbox',
			),
		),
	);

	/**
	* META FOR QUOTE POST TYPE
	*/
	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_quote',
		'title'      => 'Quote Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Quote',
				'desc' => 'i.e: Stay hungry. Stay foolish.',
				'id'   => $prefix . 'post_quote',
				'type' => 'textarea_small',
				'std' => ''
			),
			array(
				'name' => 'Author',
				'desc' => 'i.e: Steve Jobs',
				'id'   => $prefix . 'post_quote_author',
				'type' => 'text_medium',
				'std' => ''
			),
			array(
				'name' => 'Link',
				'desc' => 'i.e: www.apple.com',
				'id'   => $prefix . 'post_quote_link',
				'type' => 'text_medium',
				'std' => ''
			),
			array(
				'name' => 'Background Image',
				'desc' => 'This will be the image displayed on your link background. The image should be wide.',
				'id'   => $prefix . 'quote_bg_image',
				'type' => 'file',
				'std' => ''
			),
		)
	);
	
	/**
	* META FOR VIDEO POST TYPE
	*/
	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_video',
		'title'      => 'Video Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'MP4 File URL',
				'desc' => 'Please, upload or enter in the URL to your .mp4 video file',
				'id'   => $prefix . 'post_video_mp4',
				'type' => 'file',
				'std' => ''
			),
			array(
				'name' => 'Preview Image',
				'desc' => 'This will be the image displayed when the video has not been played yet. The image should be wide.',
				'id'   => $prefix . 'post_video_img',
				'type' => 'file',
				'std' => ''
			),
			array(
				'name' => 'Custom Embed',
				'desc' => 'If the video is an embed rather than self hosted, enter in a Youtube or Vimeo embed code here.',
				'id'   => $prefix . 'post_cs_embed',
				'type' => 'textarea_small',
				'std' => ''
			),
		)
	);
	/**
	* META FOR AUDIO POST TYPE
	*/
	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_audio',
		'title'      => 'Audio Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'MP3 File Url',
				'desc' => 'Please, upload or enter in the URL to your .mp3 audio file',
				'id'   => $prefix . 'post_audio_mp3',
				'type' => 'file',
				'std' => ''
			),
			array(
				'name' => 'Custom Embed',
				'desc' => 'If the auido is an embed rather than self hosted, enter in a soundcolud or other embed code here.',
				'id'   => $prefix . 'post_cs_sound_embed',
				'type' => 'textarea_small',
				'std' => ''
			),
		)
	);


/* ==========================================================================
PORTFOLIO
========================================================================== */

$meta_boxes[] = array(
		'id'         => 'page_metabox',
		'title'      => 'Page Meta',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Background Settings',
				'desc' => '',
				'type' => 'title',
				'id' => $prefix . 'page_bg_title'
			),
			array(
				'name' => 'Background Image',
				'desc' => 'Upload an image or enter an URL.',
				'id'   => $prefix . 'page_bg',
				'type' => 'file',
			),
			array(
	            'name' => 'Background Overlay Color',
	            'desc' => 'it will be use in your page bg overlay',
	            'id'   => $prefix . 'page_bgo_cp',
	            'type' => 'colorpicker',
				'std'  => ''
	        ),
			array(
	            'name' => 'Background Overlay Alpha',
	            'desc' => 'it will be use in your page bg overlay alpha value. it mus be 1 between 100.',
	            'id'   => $prefix . 'page_bgo_alpha',
	            'type' => 'text_small',
				'std'  => '0'
	        ),
			array(
	            'name' => 'Background Color',
	            'desc' => 'it will be use in your page detail',
	            'id'   => $prefix . 'page_bg_cp',
	            'type' => 'colorpicker',
				'std'  => 'transparent'
	        ),
		)
	);
	
	

	/**
	*  META FOR GALLERY POST TYPE
	*/
	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_gallery',
		'title'      => 'Gallery Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'fields' => array(
			array(
				'name'    => 'Gallery Type',
				'desc'    => 'please, select a gallery type',
				'id'      => $prefix . 'post_gallery_type',
				'type'    => 'select',
				'options' => array(
					array( 'name' => 'Flex Slider', 'value' => 'flex', ),
					array( 'name' => 'Grid Type', 'value' => 'grid', ),
				),
			),
			array(
				'name' => '',
				'desc' => 'Please use the sections that have appeared under the Featured Image block labeled "Second Image, Third Image..." etc to add images to your gallery.',
				'type' => 'title',
				'id' => $prefix . 'gallery_title'
			),
		),
	);
	/**
	* META FOR LINK POST TYPE
	*/
	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_link',
		'title'      => 'Link Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Link Title',
				'desc' => 'i.e: The Sweetest WordPress Themes Around.',
				'id'   => $prefix . 'post_link_title',
				'type' => 'text_medium'
			),
			array(
				'name' => 'Link',
				'desc' => 'i.e: www.themetica.com',
				'id'   => $prefix . 'post_link',
				'type' => 'text_medium'
			),
			array(
				'name' => 'Preview Image',
				'desc' => 'This will be the image displayed on your link background. The image should be wide.',
				'id'   => $prefix . 'link_bg_image',
				'type' => 'file',
				'std' => ''
			),
		)
	);

	$meta_boxes[] = array(
		'id'         => 'blogoma_post_metabox_status',
		'title'      => 'Facebook / Twitter Embed Meta',
		'pages'      => array( 'post', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Custom Embed',
				'desc' => 'If do you want to embed your tweet or facebook post. Your embed code pastes here...',
				'id'   => $prefix . 'post_social_embed',
				'type' => 'textarea_small',
				'std' => ''
			),
			array(
				'name' => 'Background Image',
				'desc' => 'Upload an image or enter an URL.',
				'id'   => $prefix . 'post_social_bg',
				'type' => 'file',
			),
			array(
	            'name' => 'Background Color',
	            'desc' => 'it will be use in your embed post preview bg',
	            'id'   => $prefix . 'post_social_bg_color',
	            'type' => 'colorpicker'
	        ),
		)
	);

	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'initialize_blogoma_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function initialize_blogoma_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) ){
		require_once 'init.php';
	}
}
