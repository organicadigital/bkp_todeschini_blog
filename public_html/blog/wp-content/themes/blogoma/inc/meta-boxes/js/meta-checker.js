jQuery(document).ready(function($){
		
	$('#post-formats-select input').change(checkFormat);
	
	function checkFormat(){
		var format = $('#post-formats-select input:checked').attr('value');
		
		if(typeof format != 'undefined'){
						
			$('#post-body div[id^=blogoma_post_metabox_]').hide();
			$('#post-body #blogoma_post_metabox_'+format+'').stop(true,true).show();
					
		}

		if( format == '0'){
			$("#post-body #blogoma_post_metabox_twitter").stop(true,true).show()
			$("#post-body #blogoma_post_metabox_facebook").stop(true,true).show()
		}	

		$("#post-body #blogoma_post_metabox_featured").show();
	}
	
	$(window).load(function(){
		checkFormat();
	})
    
});


