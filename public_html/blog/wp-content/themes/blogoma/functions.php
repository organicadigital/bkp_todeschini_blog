<?php
/**
 * blogoma functions and definitions
 *
 * @package blogoma
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
 define('BLOGOMA_TEMPLATE_PATH', get_template_directory());
 define('BLOGOMA_THEME_NAME', 'blogoma');
 define('BLOGOMA_FRAMEWORK_DIRECTORY', get_template_directory_uri(). "/inc/blogoma-admin");


if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'blogoma_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function blogoma_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on blogoma, use a find and replace
	 * to change 'blogoma' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'blogoma', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'blogoma' ),
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio',  'quote', 'link', 'status' ) );

	// Setup the WordPress core custom background feature.
	/*
	add_theme_support( 'custom-background', apply_filters( 'blogoma_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	*/

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
		'caption',
	) );
}
endif; // blogoma_setup
add_action( 'after_setup_theme', 'blogoma_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function blogoma_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'blogoma' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'blogoma_widgets_init' );


/**
* Custom Meta Boxes
*/
require_once(BLOGOMA_TEMPLATE_PATH . '/inc/meta-boxes/meta-boxes.php');

/**
* blogoma Widgets
*/
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-about-me.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-tabs.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-ads.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-dribbble.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-flickr.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-instagram.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-facebook.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-twitter.php');
require_once( BLOGOMA_TEMPLATE_PATH . '/inc/widgets/themetica-social.php');

add_image_size( 'widget-thumb', 80, 50, true ); 
add_image_size( 'blog-post', 740, 360, true ); 

add_image_size( 'blog-post-grid', 370, 340, true ); 
add_image_size( 'related-post', 220, 200, true ); 

add_image_size( 'single-post-thumbnail', 740, 360, true );


add_image_size( 'featured-full', 1115, 480, true ); 
add_image_size( 'featured-half', 565, 380, true ); 
add_image_size( 'featured-third', 370, 380, true ); 

/**
 * Add multiple thumbnail support       
 */
include( BLOGOMA_TEMPLATE_PATH . '/inc/multi-post-thumbnails/multi-post-thumbnails.php');

/**
* Options panel
*/

if (is_admin()) {
	include(BLOGOMA_TEMPLATE_PATH . '/inc/blogoma-admin/options.php');
}

global $options;
$options = get_option('blogoma_admin'); 


require_once(BLOGOMA_TEMPLATE_PATH . '/inc/shortcodes/theme-shortcodes.php');
require_once(BLOGOMA_TEMPLATE_PATH . '/inc/shortcodes/manager/tinymce.php');

/**
 * Enqueue scripts and styles.
 */
function blogoma_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array(), '2.8.1', true );

	wp_enqueue_style('pt-sans', 'http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,latin-ext');
	wp_enqueue_style('roboto-font', 'http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300&subset=latin,latin-ext');
	

	wp_enqueue_style('bootstrap' , get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('blogoma-style', get_stylesheet_uri() );
	wp_enqueue_style('blogoma' , get_template_directory_uri() . '/css/blogoma.css');
	wp_enqueue_style('flex-skin', get_stylesheet_directory_uri() . '/css/flexslider.css');
	
	wp_enqueue_script( 'blogoma-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'hoverIntent', get_template_directory_uri() . '/js/hoverIntent.js', array(), '20130311', true );
	wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.js', array(), '1.7.4', true );

	wp_enqueue_script( 'blogoma-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'blogoma-main', get_template_directory_uri() . '/js/main.js', array(), '20120518', true );
	wp_enqueue_script( 'flex-slider', get_template_directory_uri() . '/js/jquery.flexslider.js', array(), '20120518', true );
	wp_enqueue_script( 'masonry-blog', get_template_directory_uri() . '/js/isotope.min.js', array(), '2.0.0', true );
	wp_enqueue_script( 'image-loaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), '3.1.7', true );
	
	wp_enqueue_style( 'media-elem', get_template_directory_uri() . '/css/skin/mediaelementplayer.css');
	wp_enqueue_script( 'media-elem-js', get_template_directory_uri() . '/js/mediaelement-and-player.min.js', array(), '3.1.7', true );
	
	global $options;	
	
	$retina = $options['retina'];

	if($retina){
		wp_enqueue_script( 'retina', get_template_directory_uri() . '/js/retina.min.js', array(), '1.3.0', true );	
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


}

add_action( 'wp_enqueue_scripts', 'blogoma_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
//require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

//
function blogoma_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
	?>
		<?php if ( 'div' == $args['style'] ) : ?>
			<div <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<?php else : ?>
			<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<?php endif; ?>
		
		<?php if ( 'div' != $args['style'] ) : ?>
			<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
		<?php endif; ?>
			<div class="comment-author vcard">
			<div class="avatar-holder">
				<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
			</div>
			<div class="comment-author-info">
				<?php printf( __( '<cite class="fn">%s</cite><span class="says">says:</span>' ), get_comment_author_link() ); ?>
				<div class="comment-meta commentmetadata">
					<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
						<?php
							echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago';
						?>
					</a>
					<?php edit_comment_link( __( '(Edit)' ), '  ', '' );
				?>
			</div>
		</div>
		</div>
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
			<br />
		<?php endif; ?>		

		<?php comment_text(); ?>

		<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
	<?php
}


function blogoma_share(){
		
		wp_enqueue_script('blogoma_share', get_template_directory_uri().'/js/post-share.js', array('jquery'), '1.0', true );

	?>
		<a href="javascript:;" class="share-btn"><?php echo __('Share' ,'blogoma'); ?></a>

		<ul class="share-list">
			<li class="facebook">
				<a target="_blank" href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>&amp;t=<?php echo get_the_title(); ?>"><?php _e("SHARE IT", "blogoma"); ?></a>
			</li>
			<li class="twitter">
				<a target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?php echo get_permalink(); ?>&amp;shortened_url=<?php echo get_permalink(); ?>&amp;text=<?php echo get_the_title(); ?>&amp;url=<?php echo get_permalink(); ?>"><?php _e("TWEET IT", "blogoma"); ?></a>
			</li>
			<li class="pinterest">
				<?php 
					if(function_exists('blogoma_pinterest_share')) {
						echo blogoma_pinterest_share(get_the_ID()); 
					};
				?>
		    </li>
		</ul>
	
	<?php
}

function blogoma_pinterest_share($id){
  $url  = 'http://pinterest.com/pin/create/button/?source_url=' . get_permalink() . '&media=';
  
  if($id != ''){ 
	if(get_post_format() != ''){ 
		if(has_post_thumbnail($id)){
			$post_thumbnail_id = get_post_thumbnail_id($id);
			$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
			$url .= $post_thumbnail_url;
		}
	}  
  }

  $url .= '&description=' . get_the_title();
  return '<a target="_blank" href="'.$url.'">'.__("PIN IT", "blogoma").'</a>';
}

/**
 * Multi Post Thumnails 
 */
if (class_exists('MultiPostThumbnails')) {

	// Post Mult Thumbs
	new MultiPostThumbnails(
		array(
			'label' => 'Second Image',
			'id' => 'second-image',
			'post_type' => 'post'
		)
	);
	
	new MultiPostThumbnails(
		array(
			'label' => 'Third Image',
			'id' => 'third-image',
			'post_type' => 'post'
		)
	);
	
	new MultiPostThumbnails(
		array(
			'label' => 'Fourth Image',
			'id' => 'fourth-image',
			'post_type' => 'post'
		)
	);
	
	new MultiPostThumbnails(
		array(
			'label' => 'Fifth Image',
			'id' => 'fifth-image',
			'post_type' => 'post'
		)
	);
	
	new MultiPostThumbnails(
		array(
			'label' => 'Sixth Image',
			'id' => 'sixth-image',
			'post_type' => 'post'
		)
	);	
}

if ( !function_exists( 'blogoma_make_gallery' ) ) {
	function blogoma_make_gallery($postid) { 
	     
	    $size = "blog-post";

	    if (class_exists('MultiPostThumbnails')) { ?>
		  <div class="flex-slider flexslider"> 
		  	  <ul class="slides">
			    <?php if ( has_post_thumbnail() ) { echo '<li>' . get_the_post_thumbnail($postid, $size, array('title' => '')) . '</li>'; } ?>
			    <?php 
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'second-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'second-image', NULL, $size) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'third-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'third-image', NULL, $size) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fourth-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'fourth-image', NULL, $size) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fifth-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'fifth-image', NULL, $size) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'sixth-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'sixth-image', NULL, $size) . '</li>'; }
		   	    ?>
		   	   </ul>
		   </div>
		<?php } 
    	
    }
}


if ( !function_exists( 'blogoma_make_grid' ) ) {
	function blogoma_make_grid($postid) { 
	     
	    $size = "blog-post";
	    $size2 = "blog-post-grid";

	    if (class_exists('MultiPostThumbnails')) { ?>
		  <div class="grid-gallery"> 
		  	  <ul class="slides">
			    <?php if ( has_post_thumbnail() ) { echo '<li class="half">' . get_the_post_thumbnail($postid, $size2, array('title' => '')) . '</li>'; } ?>
			    <?php 
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'second-image')) { echo '<li class="half">' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'second-image', NULL, $size2) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'third-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'third-image', NULL, $size) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fourth-image')) { echo '<li class="half">' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'fourth-image', NULL, $size) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'fifth-image')) { echo '<li class="half"> ' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'fifth-image', NULL, $size2) . '</li>'; }
				   if(MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'sixth-image')) { echo '<li>' . MultiPostThumbnails::get_the_post_thumbnail(get_post_type(), 'sixth-image', NULL, $size2) . '</li>'; }
		   	    ?>
		   	   </ul>
		   </div>
		<?php } 
    	
    }
}

/**
* Post meta checker scripts
*/
function blogoma_admin_scripts() {
	wp_enqueue_script('meta-checker', get_template_directory_uri() .'/inc/meta-boxes/js/meta-checker.js', array());
}

add_action('admin_enqueue_scripts', 'blogoma_admin_scripts');


/**
*  POPULAR POST TRACK
*/
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');


function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


/**
* Custom More link
*/
function blogoma_new_excerpt_more($more) {
    global $post;
	return '<span class="moretag">...</span>';
	//'<a class="moretag" href="'. get_permalink($post->ID) . '">' .__( 'read more <i class="icon-angle-right"></i>', 'hagu' ). '</a>';
}

add_filter('excerpt_more', 'blogoma_new_excerpt_more');
add_filter('the_content_more_link', 'blogoma_new_excerpt_more');


/*  Set custom thumbnail quality ------------ */
function blogoma_thumbnail_quality( $quality ) {
    return 100;
}
add_filter( 'jpeg_quality', 'blogoma_thumbnail_quality' );
add_filter( 'wp_editor_set_quality', 'blogoma_thumbnail_quality' );


function blogoma_generate_custom_css() {
	
	$css_dir = get_stylesheet_directory() . '/css/'; 
	ob_start(); // Capture all output (output buffering)

	require($css_dir . 'custom-css.php');

	$css = ob_get_clean(); // Get generated CSS (output buffering)
	file_put_contents($css_dir . 'custom-css.css', $css, LOCK_EX); 
}

function blogoma_generate_color_css() {
	
	$css_dir = get_stylesheet_directory() . '/css/'; 
	ob_start(); // Capture all output (output buffering)

	require($css_dir . 'custom-color.php');

	$css = ob_get_clean(); // Get generated CSS (output buffering)
	file_put_contents($css_dir . 'custom-color.css', $css, LOCK_EX); 
}

function blogoma_enqueue_dynamic_css() {
	$options = get_option('blogoma_admin'); 
	
	if(!empty($options['custom_css'])) {
		wp_register_style('custom-css', get_stylesheet_directory_uri() . '/css/custom-css.css');
		wp_enqueue_style('custom-css');
	}

	if(!empty($options['theme_color'])){
		wp_register_style('custom-color', get_stylesheet_directory_uri() . '/css/custom-color.css');
		wp_enqueue_style('custom-color');
	}
} 

add_action('wp_print_styles', 'blogoma_enqueue_dynamic_css');





// CATEGORY COUNT FILTER


function blogoma_add_span_cat_count($links) {
	$links = str_replace('</a> (', '</a> <span>', $links);
	$links = str_replace(')', '</span>', $links);
	return $links;
}

add_filter('wp_list_categories', 'blogoma_add_span_cat_count');



function blogoma_set_tag_cloud_sizes($args) {
	// They are pt value dont mixed to px value...
	$args['smallest'] = 10.5;
	$args['largest'] = 16;
	return $args; 
}

add_filter('widget_tag_cloud_args','blogoma_set_tag_cloud_sizes');



// POST LIKE REGISTER
add_action('wp_ajax_nopriv_post-like', 'blogoma_post_like');
add_action('wp_ajax_post-like', 'blogoma_post_like');


function enqueue_like_post_scripts(){
	wp_enqueue_script('like_post', get_template_directory_uri().'/js/post-like.js', array('jquery'), '1.0', true );

	wp_localize_script('like_post', 'ajax_var', array(
	    'url' => admin_url('admin-ajax.php'),
	    'nonce' => wp_create_nonce("ajax-nonce")
	));
}

add_action('wp_enqueue_scripts', 'enqueue_like_post_scripts');

function blogoma_post_like()
{
    // Check for nonce security
    $nonce = $_POST['nonce'];
  
    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        die ( 'Busted!');

    if(isset($_POST['post_like']))
    {
        // Retrieve user IP address
        $ip 		= $_SERVER['REMOTE_ADDR'];
        $post_id 	= $_POST['post_id'];
         
        // Get voters'IPs for the current post
        $meta_IP 	= get_post_meta($post_id, "voted_IP");
        $voted_IP 	= $meta_IP[0];
 
        if(!is_array($voted_IP))
            $voted_IP = array();
         
        // Get votes count for the current post
        $meta_count = get_post_meta($post_id, "votes_count", true);
 
        // Use has already voted ?
        if(!blogoma_hasAlreadyVoted($post_id))
        {
            $voted_IP[$ip] = time();
 
            // Save IP and increase votes count
            update_post_meta($post_id, "voted_IP", $voted_IP);
            update_post_meta($post_id, "votes_count", ++$meta_count);
             
            // Display count (ie jQuery return value)
            echo $meta_count;
        }
        else
            echo "already";
    }
    exit;
}

function blogoma_hasAlreadyVoted($post_id)
{
    global $timebeforerevote;

    $timebeforerevote = 120; // = 2 hours
 
    // Retrieve post votes IPs
    $meta_IP = get_post_meta($post_id, "voted_IP");
    
    if ( ! isset($meta_IP[0])) {
	   $meta_IP[0] = "0";
	}
	
    $voted_IP = $meta_IP[0];
     
    if(!is_array($voted_IP))
        $voted_IP = array();
         
    // Retrieve current user IP
    $ip = $_SERVER['REMOTE_ADDR'];
     
    // If user has already voted
    if(in_array($ip, array_keys($voted_IP)))
    {
        $time = $voted_IP[$ip];
        $now = time();
         
        // Compare between current time and vote time
        if(round(($now - $time) / 60) > $timebeforerevote)
            return false;
             
        return true;
    }
     
    return false;
}

function blogoma_getPostLikeLink($post_id)
{
    $vote_count = get_post_meta($post_id, "votes_count", true);

    if(empty($vote_count)) {
		$vote_count = 0;	
	}
 
    $output = '';

    if(blogoma_hasAlreadyVoted($post_id))
        $output .= ' <span title="'.__('I like this article', "blogoma").'" class="like alreadyvoted"></span>';
    else
        $output .= '<a href="javascript:;" data-post_id="'.$post_id.'">
                    <span  title="'.__('I like this article', "blogoma").'"class="qtip like"></span>
                ';
    $output .= '<span class="count">'.$vote_count." ".__("Likes", "blogoma").' </span></a>';
     
    return $output;
}

add_filter('get_comments_number', 'blogoma_comment_count', 0);

function blogoma_comment_count( $count ) {
	global $id;
	$comments = get_approved_comments($id);
	$comment_count = 0;
	foreach($comments as $comment){
		if($comment->comment_type == ""){
			$comment_count++;
		}
	}
	return $comment_count;
}
