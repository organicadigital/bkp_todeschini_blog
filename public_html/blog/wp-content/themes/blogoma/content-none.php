<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package blogoma
 */
?>

<section class="no-results not-found">
	<div class="page-content">
		
		<h1 class="page-title"><?php _e( 'Nothing Found...', 'blogoma' ); ?></h1>

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'blogoma' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p class="no-content"><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'blogoma' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'blogoma' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
	<div class="related-post-holder suggest">
		<h2><?php echo  __( 'Things you might like', 'blogoma' ); ?></h2>
		<ul>
		<?php 
			// get sticky posts array
			$posts_per_page = 3;
        	$sticky_posts 	= get_option( 'sticky_posts' );

        	if (is_array($sticky_posts)) {

        		 $sticky_count = count($sticky_posts);
        		 if ($sticky_count > 0) {
        		 	$posts_per_page = 3 - $sticky_count;
        		 	if($posts_per_page <= 0){
        		 		$posts_per_page = 3;
        		 	}
        		 }
        	}

			$random_args = array(
				'posts_per_page' => $posts_per_page, 
				'orderby' => 'rand'   
			);

			$random_post = new WP_Query( $random_args );

			while ( $random_post->have_posts() ) : $random_post->the_post();
		?>
		
				<li>
					<?php if (has_post_thumbnail( $post->ID ) ): ?>
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'related-post' ); ?>
				        <img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo get_the_title(); ?>">
				 	<?php else: ?>
				 		<?php 
				 			// if post type has not a featured image you'll show no image placeholder
					 		$format = get_post_format();
							if ( false === $format ) {
								$format = 'standard';
							}
							if($format == "standard"){
								$post_format_img =  get_template_directory_uri()."/images/post-format-standart-bg.png";
							}
							if($format == "audio"){
								$post_format_img =  get_template_directory_uri()."/images/post-format-auido-bg.png";
							}	
							if($format == "video"){
								$post_format_img =  get_template_directory_uri()."/images/post-format-video-bg.png";
							}		
							if($format == "quote"){
								$post_format_img =  get_template_directory_uri()."/images/post-format-quote-bg.png";
							}
							if($format == "link"){
								$post_format_img =  get_template_directory_uri()."/images/post-format-link-bg.png";
							}
							if($format == "status"){
								$post_format_img =  get_template_directory_uri()."/images/post-format-status-bg.png";
							}

						?>
						<img src="<?php echo $post_format_img ?>" alt="<?php echo get_the_title(); ?>">
					<?php endif; ?>
					<div class="related-content">
				 		<span>
				 			<a href="<? the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				 		</span>
				 	</div>
				</li>	

		<?php
			endwhile;
		?>
		</ul>
	</div>
</section><!-- .no-results -->
<div class="post-paper-bg"></div>	
