<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package blogoma
 */

		$options 			= get_option('blogoma_admin'); 
		$footer				= $options['footer_show'];
		
		$social_media		= $options['social_media_show'];		

		$facebook 	= $options['facebook'];
		$twitter 	= $options['twitter'];
		$behance 	= $options['behance'];
		$dribbble 	= $options['dribbble'];
		$instagram 	= $options['instagram'];
		$pinterest 	= $options['pinterest'];
		$google 	= $options['google'];
		$youtube 	= $options['youtube'];
		$flickr 	= $options['flickr'];
		$soundcloud = $options['soundcloud'];
		$linkedin 	= $options['linkedin'];
		$rss 		= $options['rss'];

		$copyright			= $options['copyright_show'];
		$copyright_text		= $options['copyright_text'];		

		$copyright_text		= $options['copyright_text'];	


		if(isset($options["google_analytics"])){
			$analytics = $options['google_analytics'];	
		};

?>

	</div><!-- #content -->
	<?php if(!empty($footer)) : ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row">
			<div class="container">
				<div class="col-md-12">
					<?php if(!empty($social_media)) : ?>
					<div class="social-media-icons">
						<ul>
			<?php if($facebook) : ?>
				<li>
					<a href="<?php echo esc_url($facebook); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe227;" alt="Facebook"></a>
				</li>
			<?php endif; ?>
			<?php if($twitter) : ?>
				<li>
					<a href="<?php echo esc_url($twitter); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe286;" alt="Twitter"></a>
				</li>
			<?php endif; ?>
			<?php if($behance) : ?>
				<li>
					<a href="<?php echo esc_url($behance); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe209;" alt="Behance"></a>
				</li>
			<?php endif; ?>
			<?php if($dribbble) : ?>
				<li>
					<a href="<?php echo esc_url($dribbble); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe221;" alt="Dribbble"></a>
				</li>
			<?php endif; ?>
			<?php if($instagram) : ?>
				<li>
					<a href="<?php echo esc_url($instagram); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe300;" alt="Instagram"></a>
				</li>
			<?php endif; ?>
			<?php if($pinterest) : ?>
				<li>
					<a href="<?php echo esc_url($pinterest); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe264;" alt="Pinterest"></a>
				</li>
			<?php endif; ?>
			<?php if($google) : ?>
				<li>
					<a href="<?php echo esc_url($google); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe239;" alt="Google+"></a>
				</li>
			<?php endif; ?>
			<?php if($youtube) : ?>
				<li>
					<a href="<?php echo esc_url($youtube); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe299;" alt="Youtube"></a>
				</li>
			<?php endif; ?>
			<?php if($flickr) : ?>
				<li>
					<a href="<?php echo esc_url($flickr); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe229;" alt="Flickr"></a>
				</li>
			<?php endif; ?>
			<?php if($soundcloud) : ?>
				<li>
					<a href="<?php echo esc_url($soundcloud); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe278;" alt="SoundCloud"></a>
				</li>
			<?php endif; ?>
			<?php if($linkedin) : ?>
				<li>
					<a href="<?php echo esc_url($linkedin); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe252;" alt="Linkedin"></a>
				</li>
			<?php endif; ?>
			<?php if($rss) : ?>
				<li>
					<a href="<?php echo esc_url($rss); ?>" target="<?php echo esc_attr($target); ?>" class="symbol" title="&#xe271;" alt="RSS"></a>
				</li>
			<?php endif; ?>
		</ul>
					</div><!-- .site-info -->
					<?php endif; ?>
				</div>
				<div class="col-md-12">
					<?php if(!empty($copyright)) : ?>
						<div class="copy-info">
							<?php 
								echo stripslashes($copyright_text); 
							?>
						</div><!-- .site-info -->
					<?php endif; ?>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
	<?php endif; ?>
</div><!-- #page -->
	<?php 		
		if(!empty($analytics)) :
	?> 
		<script type="text/javascript">
			<?php echo $analytics; ?>
		</script>
	
	<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
