<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package blogoma
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				echo get_comments_number() ." ". __("Comments", "blogoma");
			?>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'blogoma' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'blogoma' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'blogoma' ) ); ?></div>
		</nav><!-- #comment-nav-above -->
	<?php endif; // check for comment navigation ?>

		<ol class="comment-list">
			<?php
				//wp_list_comments( array (		'style'      => 'ol',	'short_ping' => true,));

				wp_list_comments( 'type=comment&callback=blogoma_comment&avatar_size=54' );
			?>
		</ol><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'blogoma' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'blogoma' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'blogoma' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'blogoma' ); ?></p>
	<?php endif; ?>


	<?php 
		//comment_form(); 
		
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );

		$blogoma_comment_form_args = array(
			'fields' => apply_filters(
				'comment_form_default_fields', array(
					'author' => 
						'<p class="comment-form-author">' .
						'<input id="author" name="author" type="text" value="' .
						esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' placeholder="'.__( 'Your Name', 'blogoma' ).'" />' .
						'</p><!-- #form-section-author .form-section -->',
					'email'  => 
						'<p class="comment-form-email">' .
						'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
						'" size="30"' . $aria_req . ' placeholder="'.__( 'Your E-mail', 'blogoma' ).'" />' .
						'</p><!-- #form-section-email .form-section -->',
					'url' =>
					    '<p class="comment-form-url">' .
					    '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
					    '" size="30" placeholder="'.__( 'Your Website', 'blogoma' ).'" /></p>',
				)
			),
			'comment_field' => '<p class="comment-form-comment">' .
				'<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="'. __("Message","blogoma") .'"></textarea>' .
				'</p><!-- #form-section-comment .form-section -->',
		    'comment_notes_after' => '',
		 'title_reply' => __("Leave a comment", "blogoma"),
		 'comment_notes_before' => __('<p class="comment-notes">Please be polite. We appreciate that. Your email address will not be published and required fields are marked</p>', 'blogoma')
		);
		
		comment_form( $blogoma_comment_form_args );

	?>

</div><!-- #comments -->
<div class="post-paper-bg"></div>	
